# Delivery Food App
App for ordering food online created with Angular, Java, PostqreSQL.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Prerequisites](#prerequisites)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)

## General info
Food delivery app is the concept of ordering food from a restaurant or a fast food joint. App provides easy way for users to browse, order food and keep track of their order history. It also provides easy way for administrators to add and edit food, ingredients, to keep track of all order history and more. 
	
## Technologies
Project is created with:
* Angular version: 10.0.1
* Angular Material version: 10.0.1
* RxJS version: 6.6.0
* TypeScript version: 3.9.6
* Node version: 12.15.0
* Cloudinary version: 1.3.3
* ngx-pagination version: 5.0.0
* ng-gapi version: 0.0.93
* ng-toast version: 2.0.0
* Java version: 11
* PostgreSql version: 12.3

## Prerequisites
You can skip this step if you already have the prerequisites installed

* Node.js - download and install: https://nodejs.org/en/
* Git - download and install: https://git-scm.com/downloads
	
## Setup
For the application to work locally, you will need to have both Delivery API running and Delivery UI running, to run Delivery API follow the README file at this link: https://gitlab.com/Njegos97/deliveryapi

To run Delivery UI, open terminal and enter following scripts: 

* git clone https://gitlab.com/Njegos97/deliveryui.git - "Clone the repository"
* cd deliveryui - "Navigate to project root"
* npm install -g @angular/cli - "Install Angular CLI"
* npm install - "Install required dependencies"
* ng serve - "Run app locally"

* Access app locally at http://localhost:4200/ 
* Visit latest version of deployed app at https://bgs-delivery-ui.herokuapp.com/

## Features

### User Features
* Browse food
* Filter food
* Order food
* Sign in to app with your google account
* Track your order history

### Admin Features
* Add new food
* Edit existing food
* Delete food
* Add new ingredients
* Edit existing ingredients
* Filter ingredients
* Delete ingredients
* Browse users
* Change user role
* Track all order history

### To Do
* Add notifications when food is ordered
* Add infinite scroll to order history
* Rate food

## Status
Project is: in progress

## Contact
* Created by - Njegos Puljanovic
* Feel free to contact me at - njegoss997@gmail.com
* LinkedIn: https://www.linkedin.com/in/njegos-puljanovic-9a01a41ba/
