FROM nginx:1.17.8-alpine

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

COPY docker/run.sh ./run.sh

COPY docker/nginx.conf /etc/nginx/nginx.conf

COPY  /dist/delivery-ui /usr/share/nginx/html

ENTRYPOINT ["sh", "run.sh"]