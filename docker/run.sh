#!/usr/bin/env sh

mainFileNames="$(ls /usr/share/nginx/html/main*.js)";

for mainFileName in ${mainFileNames}
do
  envsubst '$BACKEND_API_URL' < ${mainFileName} > main.tmp;
  mv main.tmp ${mainFileName};
done

envsubst '$PORT' < /etc/nginx/nginx.conf > conf.tmp;
mv conf.tmp /etc/nginx/nginx.conf;

nginx -g 'daemon off;'
