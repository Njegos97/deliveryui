import { IngredientRoutingModule } from './ingredient-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IngredientFormComponent } from './ingredient-form/ingredient-form.component';
import { MaterialModule } from './../material/material.module';
import { NotifierModule} from 'angular-notifier';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  declarations: [IngredientFormComponent, IngredientListComponent],
  imports: [
    CommonModule,
    IngredientRoutingModule,
    MaterialModule,
    NotifierModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    NgxPaginationModule
  ]
})
export class IngredientModule { }
