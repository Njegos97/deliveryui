import {
  DataPage
} from './../../model/dataPage.model';
import {
  FoodService
} from './../../service/food.service';
import {
  IngredientService
} from 'src/app/service/ingredient.service';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  Ingredient
} from 'src/app/model/ingredient.model';
import {
  MatDialog
} from '@angular/material/dialog';
import {
  DeleteFoodDialogComponent
} from 'src/app/food/delete-food-dialog/delete-food-dialog.component';
import {
  User
} from 'src/app/model/user.model';
import {
  Category
} from 'src/app/enum/category.enum';
import {
  MatIconRegistry
} from '@angular/material/icon';
import {
  DomSanitizer
} from '@angular/platform-browser';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.css']
})
export class IngredientListComponent implements OnInit {

  categories = [{
    category: 'MEAT',
    isSelected: false
  }, {
    category: 'SALAD',
    isSelected: false
  }, {
    category: 'CHEESE',
    isSelected: false
  }, {
    category: 'SAUCE',
    isSelected: false
  }];
  types = [{
    name: 'BURGER',
    isSelected: false
  }, {
    name: 'PIZZA',
    isSelected: false
  }, {
    name: 'TACOS',
    isSelected: false
  }, {
    name: 'SANDWICH',
    isSelected: false
  }];
  ingredients: Array < User | Ingredient > = [];
  displayedColumns: string[] = ['delete', 'name', 'type', 'category', 'edit', 'options'];
  page = 1;
  numberOfAllIngredients: number;
  selectedTypeName = '';
  selectedCategoryName = '';

  constructor(private ingredientService: IngredientService,
              private dialog: MatDialog,
              private foodService: FoodService,
              private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer, ) {}

  ngOnInit(): void {
    this.addIcons();
    this.getIngredientsByPage(this.page);
  }

  getAllIngredients() {
    this.ingredientService.getAllIngredients().subscribe((ingredients: Ingredient[]) => {
      this.ingredients = ingredients;
    });
  }

  deleteById(ingredientId: number) {
    this.ingredientService.deleteIngredientById(ingredientId).subscribe(() => {
      this.getIngredientsByCategoryAndType(this.selectedCategoryName, this.selectedTypeName);
    });
  }

  openDialog(ingredientId: number) {
    const dialogRef = this.dialog.open(DeleteFoodDialogComponent);
    dialogRef.afterClosed().subscribe(() => {
      if (this.foodService.dialogConfirmation) {
        this.deleteById(ingredientId);
        this.foodService.dialogConfirmation = false;
      }
    });
  }

  getIngredientsByPage(page: number) {
    if (this.selectedCategoryName !== '' || this.selectedTypeName !== '') {
      this.getIngredientsByCategoryAndType(this.selectedCategoryName, this.selectedTypeName);
    } else {
      this.ingredientService.getIngredientsByPage(page - 1).subscribe((ingredients: DataPage) => {
        this.ingredients = ingredients.content;
        this.numberOfAllIngredients = ingredients.totalElements;
      });
    }
  }

  getIngredientsByCategoryAndType(category: string, type: string) {
    this.ingredientService.getIngredientsByCategoryAndType(category, type, this.page - 1).subscribe((ingredientsByCategory: DataPage) => {
      this.ingredients = ingredientsByCategory.content;
      this.numberOfAllIngredients = ingredientsByCategory.totalElements;
    });
  }

  setCategoryFilter(clickedCategory: any) {
    this.page = 1;
    if (this.checkIsFilterSelected(clickedCategory)) {
      return this.unselectSelectedCategory(clickedCategory);
    } else {
      this.getIngredientsByCategoryAndType(clickedCategory.category, this.selectedTypeName);
      this.setCategoriesArray(clickedCategory);
    }
  }

  private setCategoriesArray(clickedCategory) {
    this.categories.forEach(category => {
      if (category.category === clickedCategory.category) {
        category.isSelected = true;
        this.selectedCategoryName = category.category;
      } else {
        category.isSelected = false;
      }
    });
  }

   unselectSelectedCategory(clickedCategory) {
    this.getIngredientsByCategoryAndType('', this.selectedTypeName);
    this.selectedCategoryName = '';
    clickedCategory.isSelected = false;
  }

   checkIsFilterSelected(clickedFilter): boolean {
    if (clickedFilter.isSelected === true) {
      return true;
    }
  }

  setTypeFilter(clickedType: any) {
    this.page = 1;
    if (this.checkIsFilterSelected(clickedType)) {
      return this.unselectSelectedType(clickedType);
    } else {
      this.getIngredientsByCategoryAndType(this.selectedCategoryName, clickedType.name);
      this.setTypesArray(clickedType);
    }
  }

   unselectSelectedType(clickedType) {
    this.getIngredientsByCategoryAndType(this.selectedCategoryName, '');
    this.selectedTypeName = '';
    clickedType.isSelected = false;
  }

  private setTypesArray(clickedType) {
    this.types.forEach(type => {
      if (type.name === clickedType.name) {
        type.isSelected = true;
        this.selectedTypeName = type.name;
      } else {
        type.isSelected = false;
      }
    });
  }

  addIcons() {
    this.matIconRegistry.addSvgIcon(
      'sauce',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/sauces.svg'));
    this.matIconRegistry.addSvgIcon(
      'meat',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/meat.svg'));
    this.matIconRegistry.addSvgIcon(
      'salad',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/salad.svg'));
    this.matIconRegistry.addSvgIcon(
      'cheese',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/cheese.svg'));
    this.matIconRegistry.addSvgIcon(
      'food',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/food.svg'));
    this.matIconRegistry.addSvgIcon(
      'burger',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/hamburger.svg'));
    this.matIconRegistry.addSvgIcon(
      'tacos',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/taco.svg'));
    this.matIconRegistry.addSvgIcon(
      'pizza',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/pizza.svg'));
    this.matIconRegistry.addSvgIcon(
      'sandwich',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/sandwich.svg'));
    this.matIconRegistry.addSvgIcon(
      'food',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/food.svg'));
  }

}
