import {
  FoodService
} from './../../service/food.service';
import {
  IngredientService
} from './../../service/ingredient.service';
import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from '@angular/core/testing';

import {
  IngredientListComponent
} from './ingredient-list.component';
import {
  HttpClientTestingModule
} from '@angular/common/http/testing';
import {
  getMockIngredients,
  getMockIngredientsByType
} from 'src/app/mock/mock-ingredients';
import {
  of ,
  Observable,
  EMPTY
} from 'rxjs';
import {
  Ingredient
} from 'src/app/model/ingredient.model';
import {
  MatDialogMock
} from 'src/app/test-helpers/mat-dialog-mock';
import {
  MatDialog
} from '@angular/material/dialog';

describe('IngredientListComponent', () => {
  let component: IngredientListComponent;
  let fixture: ComponentFixture < IngredientListComponent > ;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [IngredientListComponent],
        imports: [HttpClientTestingModule],
        providers: [{
            provide: 'BACKEND_API_URL',
            useValue: baseUrl
          },
          {
            provide: MatDialog,
            useClass: MatDialogMock
          }
        ]
      })
      .overrideTemplate(IngredientListComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getAllIngredients', inject([IngredientService], (ingredientService: IngredientService) => {
    const mockIngredientList: Observable < Ingredient[] > = of (getMockIngredients());
    spyOn(ingredientService, 'getAllIngredients').and.returnValue(mockIngredientList);

    component.getAllIngredients();

    expect(ingredientService.getAllIngredients).toHaveBeenCalled();
  }));

  it('should call deleteIngredientById', inject([IngredientService], (ingredientService: IngredientService) => {
    const ingredientId = 3;
    spyOn(ingredientService, 'deleteIngredientById').and.returnValue(EMPTY);

    component.deleteById(ingredientId);

    expect(ingredientService.deleteIngredientById).toHaveBeenCalledWith(ingredientId);
  }));

  it('should call getIngredientsByPage', inject([IngredientService], (ingredientService: IngredientService) => {
    spyOn(ingredientService, 'getIngredientsByPage').and.returnValue( of (getMockIngredients()));

    component.getIngredientsByPage(1);

    expect(ingredientService.getIngredientsByPage).toHaveBeenCalledWith(0);
  }));

  it('should return ingredients by type when getIngredientsByTypeAndCategory() is called',
    inject([IngredientService], (ingredientService: IngredientService) => {
      const category = '';
      const type = 'PIZZA';
      spyOn(ingredientService, 'getIngredientsByCategoryAndType').and.returnValue( of (getMockIngredientsByType()));

      component.getIngredientsByCategoryAndType(category, type);

      expect(ingredientService.getIngredientsByCategoryAndType).toHaveBeenCalledWith(category, type, 0);
    }));

  it('should call unselectSelectedCategory when checkIsFilterSelected() returns true', () => {
    const category = {
      category: 'MEAT',
      isSelected: true
    };
    spyOn(component, 'checkIsFilterSelected').and.returnValue(true);
    spyOn(component, 'unselectSelectedCategory');

    component.setCategoryFilter(category);

    expect(component.checkIsFilterSelected).toHaveBeenCalledWith(category);
    expect(component.unselectSelectedCategory).toHaveBeenCalledWith(category);
  });

  it('should call getIngredientsByCategoryAndType when checkIsFilterSelected() returns false', () => {
    const category = {
      category: 'MEAT',
      isSelected: false
    };
    spyOn(component, 'checkIsFilterSelected').and.returnValue(false);
    spyOn(component, 'getIngredientsByCategoryAndType');

    component.setCategoryFilter(category);

    expect(component.checkIsFilterSelected).toHaveBeenCalledWith(category);
    expect(component.getIngredientsByCategoryAndType).toHaveBeenCalledWith(category.category, '');
  });

  it('should unselect selected category  when unselectSelectedCategory() is called', () => {
    const category = {
      category: 'MEAT',
      isSelected: true
    };
    spyOn(component, 'getIngredientsByCategoryAndType');
    component.selectedCategoryName = '';

    component.unselectSelectedCategory(category);

    expect(category.isSelected).toBeFalsy();
    expect(component.getIngredientsByCategoryAndType).toHaveBeenCalledWith(component.selectedCategoryName, '');
  });

  it('should return true if filter is selected', () => {
    const category = {
      category: 'MEAT',
      isSelected: true
    };

    component.checkIsFilterSelected(category);

    expect(category.isSelected).toBeTruthy();
    expect(component.checkIsFilterSelected).toBeTruthy();
  });

  it('should call unselectSelectedCategory when checkIsFilterSelected() returns true and setTypeFilter() is called', () => {
    const type = {
      name: 'BURGER',
      isSelected: true
    };
    spyOn(component, 'checkIsFilterSelected').and.returnValue(true);
    spyOn(component, 'unselectSelectedType');

    component.setTypeFilter(type);

    expect(component.checkIsFilterSelected).toHaveBeenCalledWith(type);
    expect(component.unselectSelectedType).toHaveBeenCalledWith(type);
  });

  it('should call getIngredientsByCategoryAndType when checkIsFilterSelected() returns false and setTypeFilter() is called', () => {
    const type = {
      name: 'BURGER',
      isSelected: false
    };
    spyOn(component, 'checkIsFilterSelected').and.returnValue(false);
    spyOn(component, 'getIngredientsByCategoryAndType');

    component.setTypeFilter(type);

    expect(component.checkIsFilterSelected).toHaveBeenCalledWith(type);
    expect(component.getIngredientsByCategoryAndType).toHaveBeenCalledWith('', type.name);
  });

  it('should unselect selected type  when unselectSelectedType() is called', () => {
    const type = {
      name: 'BURGER',
      isSelected: true
    };
    spyOn(component, 'getIngredientsByCategoryAndType');
    component.selectedTypeName = '';

    component.unselectSelectedType(type);

    expect(type.isSelected).toBeFalsy();
    expect(component.getIngredientsByCategoryAndType).toHaveBeenCalledWith('', component.selectedTypeName);
  });

  it('should call getIngredientsByCategoryAndType when selectedCategory or SelectedType is not empty string',
    inject([IngredientService], (ingredientService: IngredientService) => {
      spyOn(ingredientService, 'getIngredientsByCategoryAndType').and.returnValue( of (getMockIngredientsByType()));
      component.selectedTypeName = 'PIZZA';
      component.selectedCategoryName = '';

      component.getIngredientsByPage(1);

      expect(ingredientService.getIngredientsByCategoryAndType)
        .toHaveBeenCalledWith(component.selectedCategoryName, component.selectedTypeName, 0);
    }));



});
