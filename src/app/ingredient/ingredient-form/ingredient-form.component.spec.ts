import {
  getMockIngredients
} from './../../mock/mock-ingredients';
import {
  IngredientService
} from './../../service/ingredient.service';
import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from '@angular/core/testing';

import {
  IngredientFormComponent
} from './ingredient-form.component';
import {
  HttpClientTestingModule
} from '@angular/common/http/testing';
import {
  ReactiveFormsModule
} from '@angular/forms';
import {
  NotifierService,
  NotifierModule
} from 'angular-notifier';
import {
  getMockIngredientsByType
} from 'src/app/mock/mock-ingredients';
import {
  of
} from 'rxjs';
import {
  RouterTestingModule
} from '@angular/router/testing';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  RouterMock
} from 'src/app/test-helpers/router-mock';
import {
  ActivatedRouteMock
} from 'src/app/test-helpers/activated-route-mock';

describe('IngredientFormComponent', () => {
  let component: IngredientFormComponent;
  let fixture: ComponentFixture < IngredientFormComponent > ;
  const baseUrl = 'http://localhost:8080/api';
  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [IngredientFormComponent],
        imports: [HttpClientTestingModule, ReactiveFormsModule, RouterTestingModule, NotifierModule],
        providers: [{
            provide: 'BACKEND_API_URL',
            useValue: baseUrl
          },
          {
            provide: Router,
            useClass: RouterMock
          },
          {
            provide: ActivatedRoute,
            useClass: ActivatedRouteMock
          },
          {
            provide: NotifierService,
            useClass: NotifierService
          },
        ]
      })
      .overrideTemplate(IngredientFormComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should call addNewIngredient on onSubmit when isEditForm is false onSubmit', () => {
    component.isEditForm = false;
    spyOn(component, 'addNewIngredient');

    component.onSubmit();

    expect(component.addNewIngredient).toHaveBeenCalled();
  });

  it('should call editIngredient when isEditForm is true onSubmit', () => {
    component.isEditForm = true;
    spyOn(component, 'editIngredient');


    component.onSubmit();

    expect(component.editIngredient).toHaveBeenCalled();
  });

  it('should call addNewIngredient when addNewIngredient is called', inject([IngredientService],
    (ingredientService: IngredientService) => {
      spyOn(component, 'assigneIngredientFromIngredientForm').and
        .returnValue( of (getMockIngredientsByType()[0]));
      spyOn(ingredientService, 'addNewIngredient').and.returnValue( of (getMockIngredientsByType()[0]));

      component.addNewIngredient();

      expect(ingredientService.addNewIngredient).toHaveBeenCalled();
    }));

  it('should be invalid when ingredient name contains less then two letters ', () => {
    const invalidName = 'p';
    const name = component.ingredientForm.controls.name;

    name.setValue(invalidName);

    expect(name.valid).toBeFalsy();
  });

  it('should be valid when ingredient name contains more then two letters ', () => {
    const invalidName = 'Pepperoni';
    const name = component.ingredientForm.controls.name;

    name.setValue(invalidName);

    expect(name.valid).toBeTruthy();
  });

  it('should be invalid when ingredient name contains numbers ', () => {
    const invalidName = 'Pepperoni23';
    const name = component.ingredientForm.controls.name;

    name.setValue(invalidName);

    expect(name.valid).toBeFalsy();
  });

  it('should find ingredient by id when getIngredientById is called', inject([IngredientService],
    (ingredientService: IngredientService) => {
      spyOn(ingredientService, 'getIngredientById').and.returnValue( of (getMockIngredients()[0]));
      spyOn(component, 'addIngredientFormInit');

      component.findIngredientById();

      expect(ingredientService.getIngredientById).toHaveBeenCalled();
      expect(component.addIngredientFormInit).toHaveBeenCalled();
    }));

  it('should edit ingredient when editIngredient is called', inject([IngredientService],
    (ingredientService: IngredientService) => {
      component.ingredient = getMockIngredients()[0];
      component.ingredientId = 1;
      component.isEditForm = true;

      spyOn(ingredientService, 'editIngredient').and.returnValue( of (getMockIngredients()[0]));
      spyOn(component, 'assigneIngredientFromIngredientForm').and.returnValue(component.ingredient);

      component.editIngredient();

      expect(ingredientService.editIngredient).toHaveBeenCalledWith(component.ingredient, component.ingredientId);
      expect(component.ingredient).toEqual(getMockIngredients()[0]);
    }));

});
