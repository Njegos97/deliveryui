import {
  Category
} from './../../enum/category.enum';
import {
  IngredientService
} from './../../service/ingredient.service';
import {
  Ingredient
} from './../../model/ingredient.model';
import {
  Type
} from './../../enum/food.enum';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  NotifierService
} from 'angular-notifier';
import {
  ActivatedRoute,
  Params,
  Router
} from '@angular/router';

@Component({
  selector: 'app-ingredient-form',
  templateUrl: './ingredient-form.component.html',
  styleUrls: ['./ingredient-form.component.css']
})
export class IngredientFormComponent implements OnInit {

  ingredientForm: FormGroup;
  ingredient: Ingredient;
  type = Object.values(Type);
  category = Object.values(Category);
  ingredientId: number;
  isEditForm = false;

  constructor(private formBuilder: FormBuilder,
              private ingredientService: IngredientService,
              private notifierService: NotifierService,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit(): void {
    this.addIngredientFormInit();
    this.setEditForm();
  }

  setEditForm() { // set EditForm to edit or add
    this.route.params.subscribe((params: Params) => {
      if (params.hasOwnProperty('id')) {
        this.isEditForm = true;
        this.ingredientId = params.id;
        this.findIngredientById();
      } else {
        this.isEditForm = false;
      }
    });
  }

  findIngredientById() {
    this.ingredientService.getIngredientById(this.ingredientId).subscribe((ingredient: Ingredient) => {
      this.ingredient = ingredient;
      this.addIngredientFormInit();
    });
  }

  addIngredientFormInit() {
    const ONLY_LETTERS_AND_SPACES = '[a-zA-ZäöüßÄÖÜčćšđžČĆŠĐŽ ]*';
    this.ingredientForm = this.formBuilder.group({
      name: [this.isEditForm ? this.ingredient.name : '', [Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        Validators.pattern(ONLY_LETTERS_AND_SPACES)
      ]],
      type: [this.isEditForm ? this.ingredient.type : '', [Validators.required]],
      category: [this.isEditForm ? this.ingredient.category : '', [Validators.required]]

    });
  }

  assigneIngredientFromIngredientForm() {
    return this.ingredient = Object.assign(this.ingredientForm.value);
  }

  onSubmit() {
    if (this.isEditForm) {
      this.editIngredient();
    } else {
      this.addNewIngredient();
    }
  }

  addNewIngredient() {
    const newIngredient = this.assigneIngredientFromIngredientForm();
    this.ingredientService.addNewIngredient(newIngredient).subscribe(() => {
        this.notifierService.notify('success', 'Ingredient successfully added');
      },
      (error) => {
        this.notifierService.notify('error', error.message);
      });
  }

  editIngredient() {
    const editedIngrediant = this.assigneIngredientFromIngredientForm();
    this.ingredientService.editIngredient(editedIngrediant, this.ingredientId).subscribe( () => {
        this.notifierService.notify('success', 'Ingredient successfully edited');
        setTimeout(() => {
          this.router.navigate([`/ingredient-list`]);
        }, 1000);
      },
      (error) => {
        this.notifierService.notify('error', error.message);
      });
  }
}
