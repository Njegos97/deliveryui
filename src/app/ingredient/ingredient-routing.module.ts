import { IngredientListComponent } from './ingredient-list/ingredient-list.component';

import { IngredientFormComponent } from './ingredient-form/ingredient-form.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../guard/auth-guard.service';
import { RoleGuard } from '../guard/role.guard';

const routes: Routes = [
    { path: 'ingredient-form', component: IngredientFormComponent, canActivate: [RoleGuard] },
    { path: 'ingredient-list', component: IngredientListComponent, canActivate: [RoleGuard]},
    { path: 'edit-ingredient/:id', component: IngredientFormComponent, canActivate: [RoleGuard] }
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class IngredientRoutingModule { }
