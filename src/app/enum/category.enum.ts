export enum Category {
    CHEESE = 'CHEESE',
    MEAT = 'MEAT',
    SALAD = 'SALAD',
    SAUCE = 'SAUCE'
}
