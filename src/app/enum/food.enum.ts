export enum Type {
    PIZZA = 'PIZZA',
    BURGER = 'BURGER',
    TACOS = 'TACOS',
    SANDWICH = 'SANDWICH'
}
