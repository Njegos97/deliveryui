import { UserListComponent } from './user-list/user-list.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../guard/auth-guard.service';

const routes: Routes = [
    { path: 'user-profile', component: UserProfileComponent, canActivate: [AuthGuardService]},
    { path: 'user-list', component: UserListComponent, canActivate: [AuthGuardService]},
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class UserRoutingModule { }
