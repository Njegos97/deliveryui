import {
  AuthService
} from './../../service/auth.service';
import {
  UserService
} from './../../service/user.service';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  User
} from 'src/app/model/user.model';
import {
  Role
} from 'src/app/enum/role.enum';
import {
  NotifierService
} from 'angular-notifier';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  selectedUser: User;
  selectedRole: Role;
  loggedInUser: User;
  constructor(private userService: UserService, private authService: AuthService, private notifierService: NotifierService) {}

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    if (this.authService.isLoggedIn) {
      this.authService.getLoggedinUser().subscribe((user: User) => {
        this.loggedInUser = user;
        this.selectedUser = user;
      });
    }
  }

  public get profileImage(): string {
    this.authService.image = localStorage.getItem('profile_Image');
    return this.authService.image;
  }

  onValueChange(newValue) {
    this.selectedUser = newValue;
  }

  changeRole() {
    this.selectedUser.role = this.selectedRole;
    this.userService.changeUserRole(this.selectedUser).subscribe(() => {
        this.notifierService.notify('success', 'User role changed');
      },
      (error) => {
        this.notifierService.notify('error', error.message);
      });
  }

  userIsAuthorizedToChangeRole(): boolean {
    const USER_IS_AUTHORIZED_FOR_CHANGING_ROLE: boolean =
      this.loggedInUser?.role === Role.ADMIN &&
      this.loggedInUser?.id !== this.selectedUser?.id &&
      this.authService.isLoggedIn;

    if (USER_IS_AUTHORIZED_FOR_CHANGING_ROLE) {
      return true;
    }
    return false;
  }

}
