import {
  UserService
} from './../../service/user.service';
import {
  getMockUsers
} from './../../mock/mock-user';
import {
  AuthService
} from './../../service/auth.service';
import {
  async,
  ComponentFixture,
  inject,
  TestBed
} from '@angular/core/testing';

import {
  UserProfileComponent
} from './user-profile.component';
import {
  HttpClientTestingModule
} from '@angular/common/http/testing';
import {
  GoogleAuthService
} from 'ng-gapi/lib/src/GoogleAuthService';
import {
  GoogleAuthServiceMock
} from 'src/app/test-helpers/google-auth-service-mock';
import {
  NG_GAPI_CONFIG
} from 'ng-gapi/lib/src/GoogleApiService';
import {
  NgGapiClientConfig
} from 'ng-gapi/lib/src/config/GoogleApiConfig';
import {
  Router
} from '@angular/router';
import {
  RouterMock
} from 'src/app/test-helpers/router-mock';
import {
  of
} from 'rxjs';
import {
  Role
} from 'src/app/enum/role.enum';
import { NotifierModule } from 'angular-notifier';
declare global {
  interface Window {
    gapi: any;
  }
}

describe('UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture < UserProfileComponent > ;
  const baseUrl = 'http://localhost:8080/api';

  const config: NgGapiClientConfig = {
    discoveryDocs: []
  };

  beforeEach(async (() => {
    window.gapi = {
      auth2: {
        init: () => {},
        getAuth: () => {
          return {
            signIn: () => {
              return new Promise((resolve, reject) => {});

            }
          };

        }
      },
      load: () => {},
    };
    TestBed.configureTestingModule({
        declarations: [UserProfileComponent],
        imports: [HttpClientTestingModule, NotifierModule],
        providers: [{
            provide: 'BACKEND_API_URL',
            useValue: baseUrl
          }, {
            provide: GoogleAuthService,
            useClass: GoogleAuthServiceMock
          },
          {
            provide: NG_GAPI_CONFIG,
            useValue: config
          },
          {
            provide: Router,
            useClass: RouterMock
          }
        ]
      })
      .overrideTemplate(UserProfileComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getLoggedInUser()  when getUser() is called', inject([AuthService], (authService: AuthService) => {
    authService.isLoggedIn = true;
    spyOn(authService, 'getLoggedinUser').and.returnValue( of (getMockUsers()[0]));

    component.getUser();

    expect(authService.getLoggedinUser).toHaveBeenCalled();
  }));

  it('should change user role when changeRole() is called', inject([UserService], (userService: UserService) => {

    component.selectedUser = getMockUsers()[1];
    component.selectedRole = Role.ADMIN;
    component.selectedUser.role = Role.ADMIN;
    spyOn(userService, 'changeUserRole').and.returnValue( of (getMockUsers()[1]));

    component.changeRole();

    expect(userService.changeUserRole).toHaveBeenCalledWith(component.selectedUser);
    expect(component.selectedUser.role).toEqual('ADMIN');
  }));

  it('should return true if user is authorized to change role when userIsAuthorizedToChangeRole() is called',
    inject([UserService, AuthService], (userService: UserService, authService: AuthService) => {

      component.loggedInUser = getMockUsers()[0];
      component.selectedUser = getMockUsers()[1];
      authService.isLoggedIn = true;

      const isUserAuthorizedToChangeRole = component.userIsAuthorizedToChangeRole();

      expect(isUserAuthorizedToChangeRole).toEqual(true);
    }));

  it('should return false if user is not authorized to change role when userIsAuthorizedToChangeRole() is called',
    inject([UserService, AuthService], (userService: UserService, authService: AuthService) => {

      component.loggedInUser = getMockUsers()[1];
      component.selectedUser = getMockUsers()[1];
      authService.isLoggedIn = false;

      const isUserAuthorizedToChangeRole = component.userIsAuthorizedToChangeRole();

      expect(isUserAuthorizedToChangeRole).toEqual(false);
    }));

});
