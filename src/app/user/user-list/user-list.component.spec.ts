import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { getMockUsers } from 'src/app/mock/mock-user';
import { UserService } from 'src/app/service/user.service';

import { UserListComponent } from './user-list.component';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ UserListComponent ],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl}, ]
    })
    .overrideTemplate(UserListComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get user by id when getUserById() is called', inject([UserService], (userService: UserService) => {

    spyOn(userService, 'getUserById').and.returnValue(of(getMockUsers()[1]));

    component.getUser(2);

    expect(userService.getUserById).toHaveBeenCalledWith(2);
    expect(component.user).toEqual(getMockUsers()[1]);
  }));

  it('should get users by page when getUsersByPage() is called', inject([UserService], (userService: UserService) => {

    spyOn(userService, 'getUsersByPage').and.returnValue(of(getMockUsers()));

    component.getUsersByPage(1);

    expect(userService.getUsersByPage).toHaveBeenCalledWith(0);
  }));
});
