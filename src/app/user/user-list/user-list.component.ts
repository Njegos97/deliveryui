import { DataPage } from './../../model/dataPage.model';
import { UserService } from './../../service/user.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { Ingredient } from 'src/app/model/ingredient.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  @Input() user: User;
  users: Array<User | Ingredient> = [];
  @Output() valueChange = new EventEmitter();
  page = 1;
  numberOfAllUsers: number;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUsersByPage(this.page);
  }

  getUsersByPage(page: number){
    this.userService.getUsersByPage(page - 1).subscribe((users: DataPage) => {
      this.users = users.content;
      this.numberOfAllUsers = users.totalElements;
    });
  }

  getUser(id: number){
    this.userService.getUserById(id).subscribe((user: User) => {
      this.user = user;
      this.valueChange.emit(this.user);
    });
  }


}
