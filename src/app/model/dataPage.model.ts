import { Ingredient } from './ingredient.model';
import { User } from './user.model';
export interface DataPage {
    content: Array<User | Ingredient> ;
    totalElements: number;
}
