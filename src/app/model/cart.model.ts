import { Food } from './food.model';

export class Cart {
    food: Food;
    foodQuantity: number;
}
