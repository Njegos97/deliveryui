import { Type } from '../enum/food.enum';
import { Category } from '../enum/category.enum';

export class Ingredient {
    id: number;
    name: string;
    category: Category;
    type: Type;
    isSelected = true;
}
