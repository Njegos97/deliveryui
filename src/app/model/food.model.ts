import { Type } from '../enum/food.enum';

export class Food {
    id: number;
    name: string;
    price: number;
    type: Type;
    outOfStock: boolean;
    description: string;
    foodImageVersion: number;
    descriptionArray?: Array<string>;
}
