import { User } from './user.model';

export class Order {
    id: number;
    dateOfOrder: Date;
    priceOfOrder: number;
    sentTo: string;
    User: User;
}
