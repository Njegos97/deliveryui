import { Role } from './../enum/role.enum';
export class User {
    id: number;
    firstName: string;
    lastName: string;
    adrress: string;
    role: Role;
}
