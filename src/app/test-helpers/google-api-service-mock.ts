import { Observable, of } from 'rxjs';

export class GoogleApiServiceMock {
    onLoad = (): Observable<boolean> => of(true);
}
