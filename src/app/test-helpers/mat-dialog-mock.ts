import { ComponentType } from '@angular/cdk/portal';
import { MatDialogConfig } from '@angular/material/dialog';

export class MatDialogMock {
    open: <T>(component: ComponentType<T>, config: MatDialogConfig) => {};
}
