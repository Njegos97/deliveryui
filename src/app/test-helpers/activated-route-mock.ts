import { of, Observable } from 'rxjs';
import { Params } from '@angular/router';

export class ActivatedRouteMock {
    params: Observable<Params> = of( { id: 123 } );
}
