import { IngredientModule } from './../ingredient/ingredient.module';
import { MaterialModule } from './../material/material.module';
import { FoodRoutingModule } from './food-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FoodListComponent } from './food-list/food-list.component';
import { FoodFormComponent } from './food-form/food-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { DeleteFoodDialogComponent } from './delete-food-dialog/delete-food-dialog.component';
import { FoodDescriptionFormComponent } from './food-form/food-description-form/food-description-form.component';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';

import { FoodImageComponent } from './food-form/food-image/food-image.component';
import * as cloudinary from 'cloudinary-core';

const customNotifierOptions: NotifierOptions = {
  behaviour: {
    autoHide: 1000,
  }
};

@NgModule({
  declarations: [FoodListComponent, FoodFormComponent, DeleteFoodDialogComponent, FoodDescriptionFormComponent, FoodImageComponent],
  exports: [
    FoodListComponent
  ],
  imports: [
    CommonModule,
    FoodRoutingModule,
    ReactiveFormsModule,
    IngredientModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule,
    NotifierModule.withConfig(customNotifierOptions),
    CloudinaryModule.forRoot(cloudinary, {cloud_name: 'bgs-delivery'}), ]

})
export class FoodModule { }
