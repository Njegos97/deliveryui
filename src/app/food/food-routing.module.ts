import { RoleGuard } from './../guard/role.guard';
import { AuthGuardService } from './../guard/auth-guard.service';
import { FoodFormComponent } from './food-form/food-form.component';
import { FoodListComponent } from './food-list/food-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: 'food-list', component: FoodListComponent, },
    { path: 'add-food', component: FoodFormComponent, canActivate: [RoleGuard] },
    { path: 'edit-food/:id', component: FoodFormComponent, canActivate: [RoleGuard]}
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class FoodRoutingModule { }
