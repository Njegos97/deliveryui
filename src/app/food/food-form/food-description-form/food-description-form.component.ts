import { FormGroup, FormBuilder } from '@angular/forms';
import { Type } from 'src/app/enum/food.enum';


import { Component, OnInit, Input } from '@angular/core';
import { Ingredient } from 'src/app/model/ingredient.model';
import { IngredientService } from 'src/app/service/ingredient.service';
import { Food } from 'src/app/model/food.model';

@Component({
  selector: 'app-food-description-form',
  templateUrl: './food-description-form.component.html',
  styleUrls: ['./food-description-form.component.css']
})
export class FoodDescriptionFormComponent implements OnInit {


  ingredientsByType: Ingredient[];
  @Input() type: Type[];
  @Input() ingredients = [];
  @Input() foodDescriptionForm: FormGroup;
  @Input() food: Food;
  @Input() isEditForm: boolean;

  constructor(private ingredientService: IngredientService) { }

  ngOnInit(): void {
  }

   getIngredientsByType(typeId: number){
    this.ingredientService.getIngredientsByType(this.type[typeId]).subscribe(
      (ingredients: Ingredient[]) => {
        this.ingredientsByType = ingredients;
        this.setDescriptionIngredientsForEditForm(typeId);
      });
  }

  setDescriptionIngredientsForEditForm(typeId: number){
    if (this.isEditForm){
      this.ingredients = this.food.description.split(',').map(item => {
        return item.trim();
      });
      this.emptyIngredientsArrayIfTypeOfFoodIsChanged(this.type[typeId]);
      this.setSelectedIngredientForEditForm();
      }
  }

  setSelectedIngredientForEditForm(){
    this.ingredientsByType.forEach(ingredient => {
      if (this.ingredients.indexOf(ingredient.name) >= 0){
        ingredient.isSelected = true;
      }
      else{
        ingredient.isSelected = false;
      }
    });
  }

  emptyIngredientsArrayIfTypeOfFoodIsChanged(type: Type){
    if (type !== this.food.type){
      this.ingredients = [];
    }
  }

  addIngredientNameToIngredients(ingredientName){
    if (this.ingredients.includes(ingredientName.name)){
      this.ingredients.splice(this.ingredients.indexOf(ingredientName.name), 1);
      ingredientName.isSelected = false;
    }
    else{
      this.removeEmptyStringFromIngredientsArray();
      ingredientName.isSelected = true;
      this.ingredients.push(ingredientName.name);
    }
  }

  removeEmptyStringFromIngredientsArray(){
    if (this.ingredients[0] === ''){
      this.ingredients.splice(this.ingredients[0]);
    }
  }


}
