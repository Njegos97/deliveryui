import { Ingredient } from './../../../model/ingredient.model';
import { of } from 'rxjs';
import { IngredientService } from './../../../service/ingredient.service';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import {FoodDescriptionFormComponent } from './food-description-form.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Type } from 'src/app/enum/food.enum';
import { getMockIngredientsByType } from 'src/app/mock/mock-ingredients';

describe('Food Description Component', () => {
  let component: FoodDescriptionFormComponent;
  let fixture: ComponentFixture<FoodDescriptionFormComponent>;
  const baseUrl = 'http://localhost:8080/api';
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodDescriptionFormComponent ],
      imports: [HttpClientTestingModule, ],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl}, ]
    })
    .overrideTemplate(FoodDescriptionFormComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodDescriptionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should add to ingredients', () => {
    component.ingredients = [];
    const ingredient: Ingredient = getMockIngredientsByType()[0];

    component.addIngredientNameToIngredients(ingredient);

    expect(component.ingredients.length).toEqual(1);
  });

});
