import { Ingredient } from 'src/app/model/ingredient.model';
import {
  async,
  fakeAsync
} from '@angular/core/testing';
import {
  FoodImageComponent
} from './food-image/food-image.component';
import {
  FoodDescriptionFormComponent
} from './food-description-form/food-description-form.component';
import {
  FoodService
} from './../../service/food.service';
import {
  Component,
  OnInit,
  ViewChild,
  Input
} from '@angular/core';
import {
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';
import {
  Food
} from 'src/app/model/food.model';
import {
  Type
} from 'src/app/enum/food.enum';
import {
  NotifierService
} from 'angular-notifier';
import {
  ActivatedRoute,
  Params,
  Router
} from '@angular/router';
import {
  STEPPER_GLOBAL_OPTIONS
} from '@angular/cdk/stepper';

@Component({
  selector: 'app-food-form',
  templateUrl: './food-form.component.html',
  styleUrls: ['./food-form.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {
      displayDefaultIndicatorType: false
    }
  }]
})
export class FoodFormComponent implements OnInit {

  @ViewChild(FoodDescriptionFormComponent) foodDescriptionForm: FoodDescriptionFormComponent;
  @ViewChild(FoodImageComponent) foodImage: FoodImageComponent;
  foodForm: FormGroup;
  food: Food;
  foodId: number;
  isEditForm = false;
  type = Object.values(Type);
  foodDescription: string;
  ingredients = [];
  foodImageForm: FormGroup;
  foodImageFileUrl: string | ArrayBuffer = '';

  constructor(private foodService: FoodService,
              private notifierService: NotifierService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router,
  ) {}

  ngOnInit(): void {
    this.addFoodFormInit();
    this.setEditForm();
  }

  callGetIngredientsByType(typeId: number) {
    this.foodDescriptionForm.getIngredientsByType(typeId);
  }

  setEditForm() { // set FoodForm to edit or add
    this.route.params.subscribe((params: Params) => {
      if (params.hasOwnProperty('id')) {
        this.isEditForm = true;
        this.foodId = params.id;
        this.findFoodById();
      } else {
        this.isEditForm = false;
      }
    });

  }

  findFoodById() {
    this.foodService.getFoodById(this.foodId).subscribe(
      (food: Food) => {
        this.food = food;
        this.addFoodFormInit();
      });
  }

  addFoodFormInit() {
    const ONLY_LETTERS_AND_SPACES = '[a-zA-ZäöüßÄÖÜčćšđžČĆŠĐŽ ]*';
    const ONLY_NUMBERS = '^[0-9]+(.[0-9]{0,2})?$';
    this.foodForm = this.formBuilder.group({
      name: [this.isEditForm ? this.food.name : '', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        Validators.pattern(ONLY_LETTERS_AND_SPACES)
      ]],
      price: [this.isEditForm ? this.food.price : '', [
        Validators.required,
        Validators.pattern(ONLY_NUMBERS)
      ]],
      type: [this.isEditForm ? this.food.type : '', [Validators.required]],
      outOfStock: [this.isEditForm ? this.food.outOfStock : ''],
      foodDescriptionForm: this.formBuilder.group({
        foodDescription: ['']
      }),

    });
    this.buildFormForFoodImage();
    this.getIngredientsForEditForm();
  }

  buildFormForFoodImage() {
    if (this.isEditForm) {
      this.foodImageForm = this.formBuilder.group({
        foodImageFileUrl: [this.isEditForm ? this.foodImageFileUrl : '']
      });
    } else {
      this.foodImageForm = this.formBuilder.group({
        foodImageFileUrl: [this.isEditForm ? this.foodImageFileUrl : '', [Validators.required]]
      });
    }
  }

  getIngredientsForEditForm(){
    if (this.isEditForm){
    this.callGetIngredientsByType(this.type.indexOf(this.food.type));
    }
  }

  onSubmit() {
    if (this.isEditForm) {
      this.addDescriptionForFood();
      this.editFood();
      this.ingredients = [];
    } else {

      this.addDescriptionForFood();
      this.addNewFood();
      this.ingredients = [];
    }
  }

  assignFoodFromFoodForm(): Food {
    return this.food = Object.assign(this.foodForm.value);
  }

  addNewFood() {
    const newFood = this.assignFoodFromFoodForm();
    this.foodImage.showSpinner = false;
    this.foodService.saveFoodImage(this.foodImage.foodImageFileUrl, newFood.name).subscribe(() => {
      this.foodService.addNewFood(newFood).subscribe((response) => {
          this.notifierService.notify('success', 'Food successfully added');
          this.foodImage.showSpinner = true;
          this.router.navigate([`/food-list`]);
        },
        (error) => {
          this.foodImage.showSpinner = false;
          this.notifierService.notify('error', error.message);
        });
    });
    this.notifierService.notify('warning', 'This may take a few seconds');


  }

  editFood() {
    const editedFood = this.assignFoodFromFoodForm();
    this.foodImage.showSpinner = false;
    this.notifierService.notify('warning', 'This may take a few seconds');
    this.editFoodWhenImageIsNotChanged(editedFood);
    this.editFoodWithNewImage(editedFood);

  }

  editFoodWithNewImage(food: Food) {
    if (this.foodImage.foodImageFileUrl !== '') {
      this.foodService.saveFoodImage(this.foodImage.foodImageFileUrl, food.name).subscribe(() => {
        this.foodService.editFood(food, this.foodId, true).subscribe(() => {
            this.notifierService.notify('success', 'Food successfully edited');
            this.foodImage.showSpinner = true;
            this.router.navigate([`/food-list`]);
          },
          (error) => {
            this.foodImage.showSpinner = true;
            this.notifierService.notify('error', error.message);
          });
      });
    }
  }

  editFoodWhenImageIsNotChanged(food: Food) {
    if (this.foodImage.foodImageFileUrl === '') {
      this.foodService.editFood(food, this.foodId, false).subscribe(() => {
          this.notifierService.notify('success', 'Food successfully edited');
          this.foodImage.showSpinner = true;
          this.router.navigate([`/food-list`]);
        },
        (error) => {
          this.foodImage.showSpinner = true;
          this.notifierService.notify('error', error.message);
        });
    }
  }
  addDescriptionForFood() {
    this.foodDescription = this.foodDescriptionForm.ingredients.join(', ');
    this.foodForm.value.description = this.foodDescription;
  }

}
