import { ActivatedRouteMock } from './../../test-helpers/activated-route-mock';
import { RouterMock } from 'src/app/test-helpers/router-mock';
import { NotifierMocks } from './../../test-helpers/notifier- mocks';
import { MatDialogMock } from './../../test-helpers/mat-dialog-mock';
import { FoodService } from './../../service/food.service';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { FoodFormComponent } from './food-form.component';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { getMockFoodList } from 'src/app/mock/mock-food-list';
import { of } from 'rxjs';
import { NotifierService, NotifierModule } from 'angular-notifier';
import { MatDialog } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Router } from '@angular/router';

describe('FoodFormComponent', () => {
  let component: FoodFormComponent;
  let fixture: ComponentFixture<FoodFormComponent>;
  const baseUrl = 'http://localhost:8080/api';

  const FoodDescriptionFormComponentMock = jasmine.createSpyObj('FoodDescriptionFormComponent', ['getIngredientsByType']);
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ReactiveFormsModule, NotifierModule],
      declarations: [ FoodFormComponent ],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl},
                  {provide: NotifierService, useClass: NotifierService},
                  {provide: MatDialog, useClass: MatDialogMock},
                  { provide: Router, useClass: RouterMock },
                  { provide: ActivatedRoute, useClass: ActivatedRouteMock },
               ]
    })

    .overrideTemplate(FoodFormComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodFormComponent);
    component = fixture.componentInstance;
    component.foodForm = new FormGroup({});
    component.food = getMockFoodList()[0];
    fixture.detectChanges();
  });

  it('should be inavlid when name contains numbers ', () => {
    const invalidName = 'pizza1';
    const name = component.foodForm.controls.name;

    name.setValue(invalidName);

    expect(name.valid).toBeFalsy();
  });

  it('should be inavlid when name contains less then two letters ', () => {
    const invalidName = 'p';
    const name = component.foodForm.controls.name;

    name.setValue(invalidName);

    expect(name.valid).toBeFalsy();
  });

  it('should return true if name is valid', () => {
    const validName = 'Papparoni Pizza';
    const name = component.foodForm.controls.name;

    name.setValue(validName);

    expect(name.valid).toBeTruthy();
  });

  it('should return true if price is valid', () => {
    const validPrice = 5.5;
    const price = component.foodForm.controls.price;

    price.setValue(validPrice);

    expect(price.valid).toBeTruthy();
  });

  it('should return false if price is invalid', () => {
    const invalidPrice = 'a5.5';
    const price = component.foodForm.controls.price;

    price.setValue(invalidPrice);

    expect(price.valid).toBeFalsy();
  });

  xit('should call addNewFood()  when onSubmit() is called', () => {
    spyOn(component, 'addNewFood');

    component.onSubmit();

    expect(component.addNewFood).toHaveBeenCalled();
  });

  xit('should call addNewFood when is called', inject([FoodService],
    (foodService: FoodService) => {
   component.food = getMockFoodList()[0];
   component.foodId = 1;
   component.isEditForm = true;
   spyOn(component, 'assignFoodFromFoodForm').and.returnValue(component.food);
   spyOn(foodService, 'addNewFood').and.returnValue(of(getMockFoodList()[0]));

   component.addNewFood();

   expect(foodService.addNewFood).toHaveBeenCalledWith(component.food);
  }));

  it('should call addNewFood and addDescriptionForFood when isEditForm is false onSubmit', () => {
    component.isEditForm = false;
    spyOn(component, 'addDescriptionForFood');
    spyOn(component, 'addNewFood').and.returnValue();

    component.onSubmit();

    expect(component.addDescriptionForFood).toHaveBeenCalled();
    expect(component.addNewFood).toHaveBeenCalled();
  });

  it('should call editFood and addDescriptionForFood when isEditForm is true onSubmit', () => {
    component.isEditForm = true;
    spyOn(component, 'addDescriptionForFood').and.returnValue();
    spyOn(component, 'editFood').and.returnValue();

    component.onSubmit();

    expect(component.addDescriptionForFood).toHaveBeenCalled();
    expect(component.editFood).toHaveBeenCalled();
  });

  xit('should edit food when editFood is called', inject([FoodService],
    (foodService: FoodService) => {
      component.food = getMockFoodList()[0];
      component.foodId = 1;
      component.isEditForm = true;

      spyOn(foodService, 'editFood').and.returnValue(of(getMockFoodList()[0]));
      spyOn(component, 'assignFoodFromFoodForm').and.returnValue(component.food);

      component.editFood();

      expect(foodService.editFood).toHaveBeenCalledWith(component.food, component.foodId);
      expect(component.food).toEqual(getMockFoodList()[0]);
  }));

  it('should find food by id when getFoodById is called', inject([FoodService],
    (foodService: FoodService) => {
      spyOn(foodService, 'getFoodById').and.returnValue(of(getMockFoodList()[0]));
      spyOn(component, 'addFoodFormInit');

      component.findFoodById();

      expect(foodService.getFoodById).toHaveBeenCalled();
      expect(component.addFoodFormInit).toHaveBeenCalled();
  }));

  it('should invoke getIngredientsByType() when foodDescriptionForm is invoked', () => {
    component.foodDescriptionForm =  FoodDescriptionFormComponentMock;
    component.callGetIngredientsByType(0);
    expect(FoodDescriptionFormComponentMock.getIngredientsByType).toHaveBeenCalled();
});

  xit('should join ingredients to foodDescription', () => {
  const ingredients = ['Pepperoni', 'Ham'];
  const foodDescription = ingredients.join(' ');

  component.addDescriptionForFood();

  expect(foodDescription).toEqual('Pepperoni Ham');
});

  it('should call callGetIngredientsByType() when getIngredientsForEditForm() is invoked', () => {
  component.isEditForm =  true;
  spyOn(component, 'callGetIngredientsByType');

  component.getIngredientsForEditForm();

  expect(component.callGetIngredientsByType).toHaveBeenCalled();
});

});
