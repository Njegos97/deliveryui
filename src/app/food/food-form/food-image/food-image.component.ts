import {
  FoodService
} from './../../../service/food.service';
import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Food } from 'src/app/model/food.model';

@Component({
  selector: 'app-food-image',
  templateUrl: './food-image.component.html',
  styleUrls: ['./food-image.component.css']
})
export class FoodImageComponent implements OnInit {
  @Input() foodImageForm: FormGroup;
  @Input() foodImageFileUrl: string | ArrayBuffer;
  showSpinner = true;
  @Input() isEditForm: boolean;
  @Input() food: Food;
  showImage = false;
  constructor() {}

  ngOnInit(): void {}

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.showImage = true;
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (image) => {
        this.foodImageFileUrl = image.target.result;
      };
    }
  }

}
