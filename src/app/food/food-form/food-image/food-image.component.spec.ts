import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { FoodImageComponent } from './food-image.component';

describe('FoodImageComponent', () => {
  let component: FoodImageComponent;
  let fixture: ComponentFixture<FoodImageComponent>;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ FoodImageComponent ],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl}, ]
    })
    .overrideTemplate(FoodImageComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
