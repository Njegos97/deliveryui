import { FoodService } from './../../service/food.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delete-food-dialog',
  templateUrl: './delete-food-dialog.component.html',
  styleUrls: ['./delete-food-dialog.component.css']
})
export class DeleteFoodDialogComponent implements OnInit {

  constructor(private foodService: FoodService) { }

  ngOnInit(): void {
  }

  onDialogConfirmation(){
  return this.foodService.dialogConfirmation = true;
  }
}
