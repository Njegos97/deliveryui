import { FoodService } from './../../service/food.service';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { DeleteFoodDialogComponent } from './delete-food-dialog.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('DeleteFoodDialogComponent', () => {
  let component: DeleteFoodDialogComponent;
  let fixture: ComponentFixture<DeleteFoodDialogComponent>;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      declarations: [ DeleteFoodDialogComponent ],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl}, ]
    })
    .overrideTemplate(DeleteFoodDialogComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteFoodDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set dialogConfirmation to true when onDialogConfirmation method is called',
  inject([FoodService], (foodService: FoodService) => {
    spyOn(component, 'onDialogConfirmation').and.returnValue(foodService.dialogConfirmation = true);

    component.onDialogConfirmation();

    expect(component.onDialogConfirmation).toHaveBeenCalled();
    expect(foodService.dialogConfirmation).toEqual(true);
  }));
});
