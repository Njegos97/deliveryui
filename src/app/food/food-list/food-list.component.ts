import {
  AuthService
} from './../../service/auth.service';
import {
  SharedService
} from './../../service/shared.service';
import {
  DeleteFoodDialogComponent
} from './../delete-food-dialog/delete-food-dialog.component';
import {
  FoodService
} from './../../service/food.service';
import {
  Food
} from './../../model/food.model';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  MatDialog
} from '@angular/material/dialog';
import {
  Cart
} from 'src/app/model/cart.model';
import {
  User
} from 'src/app/model/user.model';
import {
  Type
} from 'src/app/enum/food.enum';
import {
  MatIconRegistry
} from '@angular/material/icon';
import {
  DomSanitizer
} from '@angular/platform-browser';


@Component({
  selector: 'app-food-list',
  templateUrl: './food-list.component.html',
  styleUrls: ['./food-list.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class FoodListComponent implements OnInit {
  selectedFilter = '';
  food: Food[];
  cart: Cart[] = [];
  isNewFood = true;
  showButtonAtIndex: number = null;
  numberOfCartItems = 0;
  isUserAdmin = false;

  constructor(private foodService: FoodService,
              private dialog: MatDialog,
              private sharedService: SharedService,
              private authService: AuthService,
              private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
  ) {
    this.addIcons();
  }

  ngOnInit(): void {
    this.getAll();
    this.sharedService.cartFood.subscribe(cart => this.cart = cart);
    if (this.authenticated) {
      this.getLoggedinUser();
    }
  }

  addToCart(food: Food) {
    this.setUpOrder(food);
    this.sharedService.addToCart(this.cart);
    this.incrementCartItemsNumber();
    this.sharedService.incrementCartItemsNumber();

  }

  incrementCartItemsNumber() {
    this.numberOfCartItems++;
  }

  setUpOrder(food: Food) {
    this.isNewFood = true;
    this.increaseFoodQuantityIfCartContainsFood(food);
    if (this.isNewFood) {
      this.insertNewFoodInCart(food);
    }
  }

  private increaseFoodQuantityIfCartContainsFood(food: Food) {
    for (const content of this.cart) {
      if (content.food.id === food.id) {
        content.foodQuantity++;
        this.isNewFood = false;
        break;
      }
    }
  }

  private insertNewFoodInCart(newFood: Food) {
    this.cart.push({
      food: newFood,
      foodQuantity: 1
    });
  }

  getAll() {
    this.foodService.getAll().subscribe(
      (response: Food[]) => {
        this.food = response;

      }
    );
  }

  onDelete(foodId: number) {
    this.foodService.deleteFood(foodId).subscribe(() => {
      if (this.selectedFilter === '') {
        this.getAll();
      } else {
        this.getFoodByType(this.selectedFilter);
      }
    });
  }

  openDialog(foodId: number) {
    const dialogRef = this.dialog.open(DeleteFoodDialogComponent);
    dialogRef.afterClosed().subscribe(() => {
      if (this.foodService.dialogConfirmation) {
        this.onDelete(foodId);
        this.foodService.dialogConfirmation = false;
      }
    });
  }

  mouseEnter(index: number) {
    this.showButtonAtIndex = index;
  }

  mouseLeave() {
    this.showButtonAtIndex = null;
  }

  getLoggedinUser() {
    this.authService.getLoggedinUser().subscribe((user: User) => {
      this.setIsUserAdmin(user);
    });
  }

  setIsUserAdmin(user: User) {
    if (user.role === 'ADMIN') {
      this.isUserAdmin = true;
    }
  }

  public get authenticated(): boolean {
    return this.authService.isLoggedIn;
  }

  getFoodByType(type: string) {
    this.foodService.getFoodByType(type).subscribe((foodByType: Food[]) => {
      this.food = foodByType;
    });
  }

  addIcons() {
    this.matIconRegistry.addSvgIcon(
      'burger',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/hamburger.svg'));
    this.matIconRegistry.addSvgIcon(
      'taco',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/taco.svg'));
    this.matIconRegistry.addSvgIcon(
      'pizza',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/pizza.svg'));
    this.matIconRegistry.addSvgIcon(
      'sandwich',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/sandwich.svg'));
    this.matIconRegistry.addSvgIcon(
      'food',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/food.svg'));
  }

}
