import { SharedService } from './../../service/shared.service';
import { FoodService } from './../../service/food.service';
import { getMockFoodList, getMockFoodListByType } from '../../mock/mock-food-list';
import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FoodListComponent } from './food-list.component';
import { Observable, of, EMPTY } from 'rxjs';
import { Food } from 'src/app/model/food.model';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogMock } from 'src/app/test-helpers/mat-dialog-mock';
import { RouterTestingModule } from '@angular/router/testing';
import { GoogleAuthService } from 'ng-gapi/lib/src/GoogleAuthService';
import { GoogleAuthServiceMock } from 'src/app/test-helpers/google-auth-service-mock';
import { NgGapiClientConfig } from 'ng-gapi/lib/src/config/GoogleApiConfig';
import { NG_GAPI_CONFIG } from 'ng-gapi/lib/src/GoogleApiService';
import { Router } from '@angular/router';
import { RouterMock } from 'src/app/test-helpers/router-mock';

describe('FoodListComponent', () => {
  let component: FoodListComponent;
  let fixture: ComponentFixture<FoodListComponent>;
  const baseUrl = 'http://localhost:8080/api';

  const config: NgGapiClientConfig = {
    discoveryDocs: []
  };

  beforeEach(async(() => {
    window.gapi = {
      auth2: {
        init: () => {},
        getAuth: () => {
          return {
            signIn: () => {
              return new Promise((resolve, reject) => {});

            }
          };

        }
      },
      load: () => {},
    };
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      declarations: [ FoodListComponent ],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl},
                  {provide: MatDialog, useClass: MatDialogMock},
                  {
                    provide: GoogleAuthService,
                    useClass: GoogleAuthServiceMock
                  },
                  {
                    provide: NG_GAPI_CONFIG,
                    useValue: config
                  },
                  {
                    provide: Router,
                    useClass: RouterMock
                  }]
    })
    .overrideTemplate(FoodListComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return list of all food when getAll() is called', inject([FoodService],
    (foodService: FoodService) => {
    const mockFoodList: Observable<Food[]> = of(getMockFoodList());

    spyOn(foodService, 'getAll').and.returnValue(mockFoodList);
    component.getAll();

    expect(foodService.getAll).toHaveBeenCalled();
  }));

  it('should call deleteFood service method when onDelete is called', inject([FoodService],
    (foodService: FoodService) => {
    const deleteFoodWithId = 1;
    spyOn(foodService, 'deleteFood').and.returnValue(EMPTY);

    component.onDelete(deleteFoodWithId);

    expect(foodService.deleteFood).toHaveBeenCalledWith(deleteFoodWithId);
  }));

  it('should call getAll when onDelete is called with selectedFilter empty', inject([FoodService],
    (foodService: FoodService) => {
    const deleteFoodWithId = 1;
    spyOn(foodService, 'deleteFood').and.returnValue(of(getMockFoodList));
    spyOn(component, 'getAll');
    component.selectedFilter = '';

    component.onDelete(deleteFoodWithId);

    expect(foodService.deleteFood).toHaveBeenCalledWith(deleteFoodWithId);
    expect(component.getAll).toHaveBeenCalled();
  }));


  it('should call setUpOrder when addToCart is called', inject([SharedService],
    (sharedService: SharedService) => {
    const food = getMockFoodList[0];
    spyOn(component, 'setUpOrder');
    spyOn(sharedService, 'addToCart');

    component.addToCart(food);

    expect(component.setUpOrder).toHaveBeenCalledWith(food);
    expect(sharedService.addToCart).toHaveBeenCalledWith(component.cart);
  }));

  it('should return list of food by type when getFoodByType() is called', inject([FoodService],
    (foodService: FoodService) => {
    const type = 'BURGER';
    const mockFoodListByType: Observable<Food[]> = of(getMockFoodListByType());

    spyOn(foodService, 'getFoodByType').and.returnValue(mockFoodListByType);
    component.getFoodByType(type);

    expect(foodService.getFoodByType).toHaveBeenCalledWith(type);
  }));

});
