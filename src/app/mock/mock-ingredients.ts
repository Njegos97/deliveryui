import {
  Ingredient
} from './../model/ingredient.model';
import {
  Type
} from './../enum/food.enum';
import {
  Category
} from './../enum/category.enum';
import {
  DataPage
} from '../model/dataPage.model';
const mockIngredientsByType: Ingredient[] = [{
    id: 1,
    name: 'Pepperoni',
    type: Type.PIZZA,
    category: Category.MEAT,
    isSelected: true

  },
  {
    id: 2,
    name: 'Green Olives',
    type: Type.PIZZA,
    category: Category.SALAD,
    isSelected: true
  },
];

const mockIngredients: Ingredient[] = [{
    id: 1,
    name: 'Pepperoni',
    type: Type.PIZZA,
    category: Category.MEAT,
    isSelected: true

  },
  {
    id: 2,
    name: 'Ham',
    type: Type.BURGER,
    category: Category.MEAT,
    isSelected: true
  },
];

const mockEditedIngredient: Ingredient = {
  id: 1,
  name: 'Tomato',
  type: Type.BURGER,
  category: Category.SALAD,
  isSelected: true

};

const ingredientsByPage: DataPage = {
  content: [{
      id: 1,
      name: 'Pepperoni',
      type: Type.PIZZA,
      category: Category.MEAT,
      isSelected: true
    },
    {
      id: 2,
      name: 'Green Olives',
      type: Type.PIZZA,
      category: Category.SALAD,
      isSelected: true
    },
    {
      id: 1,
      name: 'Tomato',
      type: Type.BURGER,
      category: Category.SALAD,
      isSelected: true
    }
  ],
  totalElements: 3
};

export const getMockIngredientsByType = (): Ingredient[] => {
  return mockIngredientsByType;
};

export const getMockEditedIngredient = (): Ingredient => {
  return mockEditedIngredient;
};

export const getMockIngredients = (): Ingredient[] => {
  return mockIngredients;
};

export const getMockIngredientsByPage = (): DataPage => {
    return ingredientsByPage;
};
