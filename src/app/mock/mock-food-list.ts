import { Food } from '../model/food.model';
import { Type } from '../enum/food.enum';


const mockFoodList: Food[] = [
    {
        id: 1,
        name: 'Papparoni Pizza',
        price: 7.50,
        type: Type.PIZZA,
        outOfStock: false,
        description: 'Ham Pepperoni',
        foodImageVersion: 1
    },
    {
        id: 2,
        name: 'Big Mac',
        price: 6,
        type: Type.BURGER,
        outOfStock: false,
        description: 'Black Olives Parmesan',
        foodImageVersion: 1
    },
    {
        id: 3,
        name: 'Cheesburger',
        price: 7.50,
        type: Type.BURGER,
        outOfStock: true,
        description: 'Green Olives Pepperoni',
        foodImageVersion: 1
    }
];

const mockEditedFood: Food = {
        id: 1,
        name: 'Cheesburger',
        price: 6.50,
        type: Type.BURGER,
        outOfStock: false,
        description: 'Cheddar Cheese, Mustard',
        foodImageVersion: 1
};

const mockFoodListByType: Food[] = [
    {
        id: 2,
        name: 'Big Mac',
        price: 6,
        type: Type.BURGER,
        outOfStock: false,
        description: 'Black Olives Parmesan',
        foodImageVersion: 1
    },
    {
        id: 3,
        name: 'Cheesburger',
        price: 7.50,
        type: Type.BURGER,
        outOfStock: true,
        description: 'Green Olives Pepperoni',
        foodImageVersion: 1
    }
];




export const getMockFoodList = (): Food[] => {
    return mockFoodList;
};

export const getMockEditedFood = (): Food => {
    return mockEditedFood;
};

export const getMockFoodListByType = (): Food[] => {
    return mockFoodListByType;
};




