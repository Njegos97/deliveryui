import {
  Cart
} from './../model/cart.model';
import {
  Type
} from '../enum/food.enum';
const mockCart: Cart[] = [{
    food: {
      id: 1,
      name: 'Cheesburger',
      price: 6.50,
      type: Type.BURGER,
      outOfStock: false,
      description: 'Cheddar Cheese, Mustard',
      foodImageVersion: 1
    },
    foodQuantity: 2
  },
  {
    food: {
      id: 2,
      name: 'Pepperoni Pizza',
      price: 7.50,
      type: Type.PIZZA,
      outOfStock: false,
      description: 'Ham Pepperoni',
      foodImageVersion: 1
    },
    foodQuantity: 1
  }
];

export const getMockCart = (): Cart[] => {
    return mockCart;
};
