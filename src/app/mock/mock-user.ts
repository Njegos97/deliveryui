import {
  DataPage
} from './../model/dataPage.model';
import {
  Role
} from '../enum/role.enum';
import {
  User
} from './../model/user.model';
const users: User[] = [{
    id: 1,
    firstName: 'Paul',
    lastName: 'Pitterson',
    adrress: 'Smiths street',
    role: Role.ADMIN
  },
  {
    id: 2,
    firstName: 'John',
    lastName: 'Smith',
    adrress: 'Smiths street',
    role: Role.USER
  },
  {
    id: 3,
    firstName: 'Jane',
    lastName: 'Black',
    adrress: 'Blacks street',
    role: Role.USER
  }
];

const usersByPage: DataPage = {
  content: [{
      id: 1,
      firstName: 'Paul',
      lastName: 'Pitterson',
      adrress: 'Smiths street',
      role: Role.ADMIN
    },
    {
      id: 2,
      firstName: 'John',
      lastName: 'Smith',
      adrress: 'Smiths street',
      role: Role.USER
    },
    {
      id: 3,
      firstName: 'Jane',
      lastName: 'Black',
      adrress: 'Blacks street',
      role: Role.USER
    }
  ],
  totalElements: 3
};

export const getMockUsers = (): User[] => {
  return users;
};

export const getMockUsersByPage = (): DataPage => {
  return usersByPage;
};
