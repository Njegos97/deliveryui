import { Role } from '../enum/role.enum';
import {
  Order
} from './../model/order.model';

const mockOrders: Order[] = [{
    id: 1,
    dateOfOrder: new Date(),
    priceOfOrder: 22,
    User: {
      id: 1,
      firstName: 'Jane',
      lastName: 'Black',
      adrress: 'Blacks street',
      role: Role.USER
    },
    sentTo: 'Blacks street'
  },
  {
    id: 2,
    dateOfOrder: new Date(),
    priceOfOrder: 34,
    User: {
      id: 1,
      firstName: 'Paul',
      lastName: 'Pitterson',
      adrress: 'Smiths street',
      role: Role.ADMIN
    },
    sentTo: '1st street'
  },
  {
    id: 3,
    dateOfOrder: new Date(),
    priceOfOrder: 9,
    User: {
      id: 1,
      firstName: 'John',
      lastName: 'Smith',
      adrress: 'Smiths street',
      role: Role.USER
    },
    sentTo: 'Smiths street'
  },
];

export const getMockOrders = (): Order[] => {
    return mockOrders;
};
