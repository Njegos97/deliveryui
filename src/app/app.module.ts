import { UserModule } from './user/user.module';
import { OrderModule } from './order/order.module';
import { IngredientModule } from './ingredient/ingredient.module';
import { MaterialModule } from './material/material.module';
import { FoodModule } from './food/food.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
const customNotifierOptions: NotifierOptions = {
  behaviour: {
    autoHide: 1000,
  }
};
import {
  GoogleApiModule,
  NgGapiClientConfig,
  NG_GAPI_CONFIG
} from 'ng-gapi/lib/src';
import { FormsModule } from '@angular/forms';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AdminInformationDialogComponent } from './dialogs/admin-information-dialog/admin-information-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ErrorPageComponent,
    AdminInformationDialogComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FoodModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    IngredientModule,
    OrderModule,
    NotifierModule.withConfig(customNotifierOptions),
    UserModule,
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: {
        client_id: environment.googleClientId,
        discoveryDocs: ['https://analyticsreporting.googleapis.com/$discovery/rest?version=v4'],
        cookie_policy: 'single_host_origin'
      } as NgGapiClientConfig
    }),
    FormsModule,
  ],
  providers: [{provide: 'BACKEND_API_URL', useValue: environment.backendApiUrl}],
  bootstrap: [AppComponent]
})
export class AppModule { }
