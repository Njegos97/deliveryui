import { DataPage } from './../model/dataPage.model';
import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getMockUsers, getMockUsersByPage } from '../mock/mock-user';
import { User } from '../model/user.model';

describe('UserService', () => {
  let service: UserService;
  let controller: HttpTestingController;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl}, ]
    });
    service = TestBed.inject(UserService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get user by id', () => {
    service.getUserById(getMockUsers()[0].id).subscribe((user: User) => {
      expect(user).toEqual(getMockUsers()[0]);
    });
    const req = controller.expectOne(`${baseUrl}/user/${getMockUsers()[0].id}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockUsers()[0]);
  });

  it('should change user role', () => {
    service.changeUserRole(getMockUsers()[0]).subscribe((user: User) => {
      expect(user).toEqual(getMockUsers()[0]);
    });
    const req = controller.expectOne(`${baseUrl}/user/role-change`);
    expect(req.request.method).toEqual('PUT');
    req.flush(getMockUsers()[0]);
  });

  it('should return users by page', () => {
    const pageNumber = 0;
    service.getUsersByPage(pageNumber).subscribe((users: DataPage) => {
      expect(users.content).toEqual(getMockUsersByPage().content);
      expect(users.totalElements).toEqual(getMockUsersByPage().totalElements);
    });
    const req = controller.expectOne(`${baseUrl}/user?pageNumber=${pageNumber}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockUsersByPage());
  });
});
