import { RouterMock } from './../test-helpers/router-mock';
import { GoogleAuthServiceMock } from './../test-helpers/google-auth-service-mock';
import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { GoogleAuthService } from 'ng-gapi/lib/src/GoogleAuthService';

import {NgGapiClientConfig } from 'ng-gapi/lib/src/config/GoogleApiConfig';


import { Router } from '@angular/router';
import { NG_GAPI_CONFIG} from 'ng-gapi/lib/src/GoogleApiService';
declare global {
  interface Window { gapi: any; }
}
describe('AuthService', () => {
  let service: AuthService;
  const baseUrl = 'http://localhost:8080/api';

  const config: NgGapiClientConfig = {
    discoveryDocs: []
  };

  beforeEach(() => {
    window.gapi = {
      auth2: {
        init: () => {},
        getAuthInstance: () => {
          return {
            signIn: () => {
              return new Promise((resolve, reject) => {
              });

            }
          };

        }
      }
    };
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl},
      {provide: GoogleAuthService, useClass: GoogleAuthServiceMock },
       {provide: Router, useClass: RouterMock},
       {provide: NG_GAPI_CONFIG, useValue: config },
      ]
    });

    service = TestBed.inject(AuthService);

  });


});
