import {
  Injectable,
  Inject,
  NgZone
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  User
} from '../model/user.model';
import GoogleUser = gapi.auth2.GoogleUser;
import {
  NG_GAPI_CONFIG
} from 'ng-gapi/lib/src/GoogleApiService';
import {
  GoogleAuthService
} from 'ng-gapi/lib/src/GoogleAuthService';
import {
  NgGapiClientConfig,
  GoogleApiConfig
} from 'ng-gapi/lib/src/config/GoogleApiConfig';
import {
  Router
} from '@angular/router';

import GoogleAuth = gapi.auth2.GoogleAuth;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public static LOCAL_STORAGE_KEY = 'id_Token';
  isLoggedIn: boolean;
  userId: number;
  image: string;
  GoogleAuth: GoogleAuth = undefined;
  private config: GoogleApiConfig;
  token: string;

  constructor(private http: HttpClient,
              @Inject('BACKEND_API_URL') private baseUrl: string,
              private googleAuth: GoogleAuthService,
              @Inject(NG_GAPI_CONFIG) config: NgGapiClientConfig,
              private router: Router,
              private ngZone: NgZone
  ) {
    this.config = new GoogleApiConfig(config);
    this.googleInit();
  }

   sendIdToken(idToken: string) {
    return this.http.post < string > (`${this.baseUrl}/user/id-token`, idToken);
  }

  getLoggedinUser() {
    return this.http.get < User > (`${this.baseUrl}/user/logged-in`);
  }

  googleInit() {
    gapi.load('auth2', () => {
      gapi.auth2.init(this.config.getClientConfig()).then(res => {
        if (res.currentUser.get().getAuthResponse().id_token !== localStorage.getItem('id_Token')) {
          this.signOut();
        }
      });
    });
  }
  public checkForToken(): boolean {
    const token: string = localStorage.getItem(AuthService.LOCAL_STORAGE_KEY);
    if (!token) {
      this.isLoggedIn = false;
      return false;
    }
    this.isLoggedIn = true;
    return true;
  }

  public signIn() {
    this.googleAuth.getAuth()
      .subscribe((auth) => {
        auth.signIn().then(res => {
          this.signInSuccessHandler(res);
          this.isLoggedIn = true;
          this.ngZone.run(() => this.router.navigate(['/food-list']));
          window.location.reload();
        });
      });
  }

  private signInSuccessHandler(res: GoogleUser) {
    localStorage.setItem(AuthService.LOCAL_STORAGE_KEY, res.getAuthResponse().id_token);
    localStorage.setItem('profile_Image', res.getBasicProfile().getImageUrl());
  }

  signOut() {
    const auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(() => {
      this.signOutSuccessHandler();
      this.isLoggedIn = false;
      this.ngZone.run(() => this.router.navigate(['/food-list']));
    });
  }

  private signOutSuccessHandler() {
    localStorage.removeItem(AuthService.LOCAL_STORAGE_KEY);
    localStorage.removeItem('profile_Image');
  }




}
