import { Ingredient } from './../model/ingredient.model';
import {
  Type
} from 'src/app/enum/food.enum';
import {
  getMockIngredientsByType,
  getMockEditedIngredient,
  getMockIngredients,
  getMockIngredientsByPage
} from './../mock/mock-ingredients';
import {
  TestBed
} from '@angular/core/testing';

import {
  IngredientService
} from './ingredient.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import {
  DataPage
} from '../model/dataPage.model';

describe('IngredientService', () => {
  let service: IngredientService;
  let controller: HttpTestingController;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
        provide: 'BACKEND_API_URL',
        useValue: baseUrl
      }]
    });
    service = TestBed.inject(IngredientService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get ingredients by type', () => {
    const type: Type = Type.PIZZA;

    service.getIngredientsByType(type).subscribe((ingredients: Ingredient[]) => {
      expect(getMockIngredientsByType()).toEqual(ingredients);
    });
    const req = controller.expectOne(`${baseUrl}/ingredient/type?type=PIZZA`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockIngredientsByType());
  });

  it('should add new ingredient', () => {
    service.addNewIngredient(getMockIngredientsByType()[0]).subscribe((ingredient: Ingredient) => {
      expect(ingredient).toEqual(getMockIngredientsByType()[0]);
    });
    const req = controller.expectOne(`${baseUrl}/ingredient`);
    expect(req.request.method).toEqual('POST');
    req.flush(getMockIngredientsByType()[0]);
  });

  it('should update existing ingredient', () => {
    service.editIngredient(getMockEditedIngredient(), getMockIngredients()[0].id).subscribe((editedIngredient: Ingredient) => {
      expect(getMockEditedIngredient()).toEqual(editedIngredient);
    });
    const req = controller.expectOne(`${baseUrl}/ingredient/${getMockIngredients()[0].id}`);
    expect(req.request.method).toEqual('PUT');
    req.flush(getMockEditedIngredient());
  });

  it('should get Ingredient by id', () => {
    service.getIngredientById(getMockIngredients()[0].id).subscribe((ingredient: Ingredient) => {
      expect(ingredient).toEqual(getMockIngredients()[0]);
    });
    const req = controller.expectOne(`${baseUrl}/ingredient/${getMockIngredients()[0].id}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockIngredients()[0]);
  });

  it('should return list of all ingredients', () => {
    service.getAllIngredients().subscribe((ingredients: Ingredient[]) => {
      expect(ingredients).toEqual(getMockIngredients());
      expect(ingredients.length).toEqual(getMockIngredients().length);
    });
    const req = controller.expectOne(`${baseUrl}/ingredient`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockIngredients());
  });

  it('should delete existing ingredient', () => {
    service.deleteIngredientById(getMockIngredients()[0].id).subscribe((deletedIngredient: Ingredient) => {
      expect(getMockIngredients()[0]).toEqual(deletedIngredient);
    });
    const req = controller.expectOne(`${baseUrl}/ingredient/${getMockIngredients()[0].id}`);
    expect(req.request.method).toEqual('DELETE');
    req.flush(getMockIngredients()[0]);
  });

  it('should return ingredients by page', () => {
    const pageNumber = 0;
    service.getIngredientsByPage(pageNumber).subscribe((ingredients: DataPage) => {
      expect(ingredients.content).toEqual(getMockIngredientsByPage().content);
      expect(ingredients.totalElements).toEqual(getMockIngredientsByPage().totalElements);
    });
    const req = controller.expectOne(`${baseUrl}/ingredient/page?pageNumber=${pageNumber}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockIngredientsByPage());
  });

  it('should return ingredients by page, type and category', () => {
    const pageNumber = 0;
    const type = 'PIZZA';
    const category = '';

    service.getIngredientsByCategoryAndType(category, type, pageNumber).subscribe((ingredients: Ingredient[]) => {
      expect(ingredients).toEqual(getMockIngredientsByType());
    });
    const req = controller.expectOne(`${baseUrl}/ingredient/category?pageNumber=${pageNumber}&category=${category}&type=${type}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockIngredientsByType());
  });

});
