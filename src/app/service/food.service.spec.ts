import { getMockFoodListByType } from './../mock/mock-food-list';
import { getMockFoodList, getMockEditedFood} from '../mock/mock-food-list';
import { TestBed } from '@angular/core/testing';

import { FoodService } from './food.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Food } from '../model/food.model';

describe('FoodService', () => {
  let service: FoodService;
  let controller: HttpTestingController;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl}]
    });
    service = TestBed.inject(FoodService);
    controller = TestBed.inject(HttpTestingController);
  });

  afterEach(() => controller.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return list of all food when correct get request is send', () => {
    service.getAll().subscribe((foodList: Food[]) => {
      expect(foodList).toEqual(getMockFoodList());
      expect(foodList.length).toEqual(getMockFoodList().length);
    });
    const req = controller.expectOne(`${baseUrl}/food`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockFoodList());
  });

  it('should add new food', () => {
    service.addNewFood(getMockFoodList()[0]).subscribe((food: Food) => {
      expect(food).toEqual(getMockFoodList()[0]);
    });
    const req = controller.expectOne(`${baseUrl}/food`);
    expect(req.request.method).toEqual('POST');
    req.flush(getMockFoodList()[0]);
  });

  it('should get food by id', () => {
    service.getFoodById(getMockFoodList()[0].id).subscribe((food: Food) => {
      expect(food).toEqual(getMockFoodList()[0]);
    });
    const req = controller.expectOne(`${baseUrl}/food/${getMockFoodList()[0].id}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockFoodList()[0]);
  });

  it('should update existing food', () => {
    const isImageChanged = false;
    service.editFood(getMockEditedFood(), getMockFoodList()[0].id, false).subscribe((editedFood: Food) => {
      expect(getMockEditedFood()).toEqual(editedFood);
    });
    const req = controller.expectOne(`${baseUrl}/food/${getMockFoodList()[0].id}?isImageChanged=${isImageChanged}`);
    expect(req.request.method).toEqual('PUT');
    req.flush(getMockEditedFood());
  });

  it('should delete existing food', () => {
    service.deleteFood(getMockFoodList()[0].id).subscribe((deletedFood: Food) => {
      expect(getMockFoodList()[0]).toEqual(deletedFood);
    });
    const req = controller.expectOne(`${baseUrl}/food/${getMockFoodList()[0].id}`);
    expect(req.request.method).toEqual('DELETE');
    req.flush(getMockFoodList()[0]);
  });

  it('should return list of food by type when correct get request is send', () => {
    const type = 'BURGER';

    service.getFoodByType(type).subscribe((foodList: Food[]) => {
      expect(foodList).toEqual(getMockFoodListByType());
      expect(foodList.length).toEqual(getMockFoodListByType().length);
    });
    const req = controller.expectOne(`${baseUrl}/food/type?type=${type}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockFoodListByType());
  });

});
