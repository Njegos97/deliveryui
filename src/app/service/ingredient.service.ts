import { Ingredient } from './../model/ingredient.model';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Type } from '../enum/food.enum';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  constructor(private http: HttpClient,
              @Inject('BACKEND_API_URL') private baseUrl: string, ) { }

    getIngredientsByType(type: Type){
      return this.http.get<Ingredient[]>(`${this.baseUrl}/ingredient/type?type=${type}`);
    }

    addNewIngredient(ingredient: Ingredient){
      return this.http.post<Ingredient>(`${this.baseUrl}/ingredient`, ingredient);
    }

    getAllIngredients(){
      return this.http.get<Ingredient[]>(`${this.baseUrl}/ingredient`);
    }

    getIngredientById(ingredientId: number){
      return this.http.get<Ingredient>(`${this.baseUrl}/ingredient/${ingredientId}`);
    }

    editIngredient(ingredient: Ingredient, ingredientId: number){
      return this.http.put<Ingredient>(`${this.baseUrl}/ingredient/${ingredientId}`, ingredient);
    }

    deleteIngredientById(ingredientId: number){
      return this.http.delete(`${this.baseUrl}/ingredient/${ingredientId}`);
    }

    getIngredientsByPage(pageNumber: number){
      return this.http.get(`${this.baseUrl}/ingredient/page?pageNumber=${pageNumber}`);
    }

    getIngredientsByCategoryAndType(category: string, type: string, pageNumber: number){
      return this.http.get(`${this.baseUrl}/ingredient/category?pageNumber=${pageNumber}&category=${category}&type=${type}`);
    }
}
