import { Type } from './../enum/food.enum';
import { Cart } from './../model/cart.model';
import { Food } from './../model/food.model';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class FoodService {

  dialogConfirmation = false;

  constructor(private http: HttpClient,
              @Inject('BACKEND_API_URL') private baseUrl: string, ) { }

    getAll(){
      return this.http.get<Food[]>(`${this.baseUrl}/food`);
    }
    addNewFood(food: Food){
      return this.http.post<Food>(`${this.baseUrl}/food`, food);
    }
    deleteFood(foodId: number){
      return this.http.delete(`${this.baseUrl}/food/${foodId}`);
    }
    editFood(food: Food, foodId: number, isImageChanged: boolean){
      return this.http.put<Food>(`${this.baseUrl}/food/${foodId}?isImageChanged=${isImageChanged}`, food);
    }
    getFoodById(foodId: number){
      return this.http.get<Food>(`${this.baseUrl}/food/${foodId}`);
    }

    saveFoodImage(imageUrl: string | ArrayBuffer, foodName){
      return this.http.post<string>(`${this.baseUrl}/food/image?foodName=${foodName}`, imageUrl);
    }

    getFoodByType(type: string){
      return this.http.get<Food[]>(`${this.baseUrl}/food/type?type=${type}`);
    }

}
