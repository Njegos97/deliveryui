import {
  User
} from './../model/user.model';
import {
  Injectable,
  Inject
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient,
              @Inject('BACKEND_API_URL') private baseUrl: string, ) {}

  getUsersByPage(pageNumber: number) {
    return this.http.get(`${this.baseUrl}/user?pageNumber=${pageNumber}`);
  }
  getUserById(id: number) {
    return this.http.get < User > (`${this.baseUrl}/user/${id}`);
  }
  addNewUser(user: User) {
    return this.http.post < User > (`${this.baseUrl}/user`, user);
  }
  editUser(user: User, userId: number){
    return this.http.put<User>(`${this.baseUrl}/user/${userId}`, user);
  }
  changeUserRole(user: User){
    return this.http.put<User>(`${this.baseUrl}/user/role-change`, user);
  }




}
