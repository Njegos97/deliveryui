import { Injectable } from '@angular/core';
import { Food } from '../model/food.model';
import { BehaviorSubject, Subject } from 'rxjs';
import { Cart } from '../model/cart.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {



  private cartFoodSubject = new BehaviorSubject<Cart[]>([]);
  cartFood = this.cartFoodSubject.asObservable();
  cartItemsNumberChanged = new Subject<number>();
  number = 0;
  constructor() { }

  addToCart(cart: Cart[]){
    this.cartFoodSubject.next(cart);
  }

  resetCartItemsAfterOrder(){
    this.number = 0;
    this.cartItemsNumberChanged.next(this.number);
  }

  incrementCartItemsNumber() {
    this.number++;
    this.cartItemsNumberChanged.next(this.number);
  }

  decrementCartItemsNumber(){
    this.number--;
    this.cartItemsNumberChanged.next(this.number);
  }

  decrementCartItemsNumberBy(decrementNumber: number){
    this.number -= decrementNumber;
    this.cartItemsNumberChanged.next(this.number);
  }



}
