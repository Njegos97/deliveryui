import { getMockOrders } from './../mock/mock-order';
import { TestBed } from '@angular/core/testing';

import { OrderService } from './order.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getMockCart } from '../mock/mock-cart';
import { Cart } from '../model/cart.model';
import { Order } from '../model/order.model';

describe('OrderService', () => {
  let service: OrderService;
  let controller: HttpTestingController;
  const baseUrl = 'http://localhost:8080/api';
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl}, ]
    });
    service = TestBed.inject(OrderService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should add new order', () => {
    const orderPrice = 22;
    const sentTo = 'Liberty Street';
    service.addNewOrder(orderPrice, getMockCart(), sentTo).subscribe((cart: Cart[]) => {
      expect(cart).toEqual(getMockCart());
    });
    const req = controller.expectOne(`${baseUrl}/cart?orderPrice=${orderPrice}&sentTo=${sentTo}`);
    expect(req.request.method).toEqual('POST');
    req.flush(getMockCart());
  });

  it('should get orderContent by id', () => {
    service.getOrderContent(getMockOrders()[0].id).subscribe((orderContent: Cart) => {
      expect(orderContent).toEqual(getMockCart()[0]);
    });
    const req = controller.expectOne(`${baseUrl}/cart/${getMockOrders()[0].id}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getMockCart()[0]);
  });

});
