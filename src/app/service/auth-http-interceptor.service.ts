import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthHttpInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    let authToken;
    if ((localStorage.getItem('id_Token'))) {
      authToken = localStorage.getItem('id_Token');
      req = req.clone(
        {
          setHeaders: {
            Authorization: `Bearer ${authToken}`
          }
        }
      );
    }
    return next.handle(req);
  }

  constructor() { }
}
