import {
  Injectable,
  Inject
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Cart
} from '../model/cart.model';
import {
  Order
} from '../model/order.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient,
              @Inject('BACKEND_API_URL') private baseUrl: string, ) {}

  addNewOrder(orderPrice: number, cart: Cart[], sentTo: string) {
    return this.http.post < Cart[] > (`${this.baseUrl}/cart?orderPrice=${orderPrice}&sentTo=${sentTo}`, cart);
  }

  getOrderContent(id: number) {
    return this.http.get(`${this.baseUrl}/cart/${id}`);
  }

  getOrderHistory() {
    return this.http.get < Order[] > (`${this.baseUrl}/cart`);
  }

}
