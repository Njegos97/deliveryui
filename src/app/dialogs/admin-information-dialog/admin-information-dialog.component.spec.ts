import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInformationDialogComponent } from './admin-information-dialog.component';

describe('AdminInformationDialogComponent', () => {
  let component: AdminInformationDialogComponent;
  let fixture: ComponentFixture<AdminInformationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminInformationDialogComponent ]
    })
    .overrideTemplate(AdminInformationDialogComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInformationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
