import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './guard/auth-guard.service';
import { ErrorPageComponent } from './error-page/error-page.component';


const routes: Routes = [
 {path: '', loadChildren: () => import('./food/food.module').then(m => m.FoodModule)} ,
 {path: '', component: AppComponent},
 { path: 'unauthorized', component: ErrorPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
