import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GoogleAuthService } from 'ng-gapi/lib/src/GoogleAuthService';
import { GoogleAuthServiceMock } from '../test-helpers/google-auth-service-mock';
import { NgGapiClientConfig } from 'ng-gapi/lib/src/config/GoogleApiConfig';
import { NG_GAPI_CONFIG } from 'ng-gapi/lib/src/GoogleApiService';
import { Router } from '@angular/router';
import { RouterMock } from '../test-helpers/router-mock';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  const baseUrl = 'http://localhost:8080/api';

  const config: NgGapiClientConfig = {
    discoveryDocs: []
  };

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl},
      {provide: GoogleAuthService, useClass: GoogleAuthServiceMock },
      {provide: NG_GAPI_CONFIG, useValue: config },
      {provide: Router, useClass: RouterMock}],
      declarations: [ NavbarComponent ]
    })
    .overrideTemplate(NavbarComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
