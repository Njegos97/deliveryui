import {
  SharedService
} from './../service/shared.service';
import {
  AuthService
} from './../service/auth.service';
import {
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  MatMenuTrigger
} from '@angular/material/menu';
import {
  User
} from '../model/user.model';
import { MatDialog } from '@angular/material/dialog';
import { AdminInformationDialogComponent } from '../dialogs/admin-information-dialog/admin-information-dialog.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @ViewChild('ingredientTrigger', {
    static: false
  }) trigger: MatMenuTrigger;
  recheckIfInMenu: boolean;
  loggedIn: boolean;
  numberOfCartItems: number;
  isUserAdmin = false;

  constructor(private authService: AuthService, private sharedService: SharedService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.recheckIfInMenu = false;
    this.authService.checkForToken();
    if (this.authenticated) {
      this.getLoggedinUser();
    }
    if (!sessionStorage.getItem('dialogSeen')){
      this.openAdminInformationDialog();
      }

    this.sharedService.cartItemsNumberChanged.subscribe(numberOfCartItems => this.numberOfCartItems = numberOfCartItems);
  }

  openResourceMenu(menu: MatMenuTrigger) {
    if (this.authenticated && this.isUserAdmin) {
      menu.openMenu();
    }
  }

  closeResourceMenu(menu: MatMenuTrigger) {
    setTimeout(() => {
      if (this.recheckIfInMenu === false) {
        menu.closeMenu();
      }
    }, 175);
  }

  signIn() {
    this.authService.signIn();
  }

  getLoggedinUser() {
    this.authService.getLoggedinUser().subscribe((user: User) => {
      this.setIsUserAdmin(user);
    });
  }

  setIsUserAdmin(user: User) {
    if (user.role === 'ADMIN') {
      this.isUserAdmin = true;
    }
  }

  signOut() {
    this.authService.signOut();
  }

  public get authenticated(): boolean {
    return this.authService.isLoggedIn;
  }

  public get profileImage(): string {
    this.authService.image = localStorage.getItem('profile_Image');
    return this.authService.image;
  }

  stopClickPropagate(event) {
    event.stopPropagation();
  }

  openAdminInformationDialog(){
    this.dialog.open(AdminInformationDialogComponent);
    sessionStorage.setItem('dialogSeen', 'true');
  }

}
