import {
  User
} from './../model/user.model';
import {
  AuthService
} from './../service/auth.service';
import {
  Injectable
} from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import {
  Observable, of, pipe
} from 'rxjs';
import {
  map
} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable < boolean > {
    if (this.authService.isLoggedIn) {
      return this.isUserAdmin();
    }else{
      return this.userIsNotLoggedIn();
    }
  }

  isUserAdmin(): Observable < boolean >  {
    return this.authService.getLoggedinUser().pipe(map((signedUser: User) => {
      if (signedUser.role === 'ADMIN') {
        return true;
      } else {
        this.router.navigate(['/unauthorized']);
        return false;
      }
    }));
  }

  userIsNotLoggedIn(): Observable<boolean>{
      this.router.navigate(['/unauthorized']);
      return of(false);
  }

}
