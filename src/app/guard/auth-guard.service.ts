import { AuthService } from './../service/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  user: User;
  constructor(private authService: AuthService, private router: Router) { }

  canActivate(): boolean{
    if (!this.isAuthenticated()){
      return false;
    }
    return true;
  }

  private isAuthenticated(): boolean{
    if (!this.authService.checkForToken()){
      this.router.navigate(['/unauthorized']);
      return false;
    }
    return true;
  }
}
