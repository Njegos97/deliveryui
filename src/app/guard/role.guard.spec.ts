import {
  getMockUsers
} from './../mock/mock-user';
import {
  AuthService
} from './../service/auth.service';
import {
  HttpClientTestingModule
} from '@angular/common/http/testing';
import {
  inject,
  TestBed
} from '@angular/core/testing';
import {
  Router
} from '@angular/router';
import {
  NgGapiClientConfig
} from 'ng-gapi/lib/src/config/GoogleApiConfig';
import {
  NG_GAPI_CONFIG
} from 'ng-gapi/lib/src/GoogleApiService';
import {
  GoogleAuthService
} from 'ng-gapi/lib/src/GoogleAuthService';
import {
  loadavg
} from 'os';
import {
  GoogleAuthServiceMock
} from '../test-helpers/google-auth-service-mock';
import {
  RouterMock
} from '../test-helpers/router-mock';

import {
  RoleGuard
} from './role.guard';
import {
  of
} from 'rxjs';
declare global {
  interface Window {
    gapi: any;
  }
}

class MockRouter {
  navigate(path) {}
}

describe('RoleGuardGuard', () => {
  let roleGuard: RoleGuard;
  let guard: RoleGuard;
  let router;
  const routeMock: any = {
    snapshot: {}
  };
  const routeStateMock: any = {
    snapshot: {},
    url: '/cookies'
  };
  const baseUrl = 'http://localhost:8080/api';

  const config: NgGapiClientConfig = {
    discoveryDocs: []
  };

  beforeEach(() => {
    window.gapi = {
      auth2: {
        init: () => {},
        getAuth: () => {
          return {
            signIn: () => {
              return new Promise((resolve, reject) => {});

            }
          };

        }
      },
      load: () => {},
    };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
          provide: GoogleAuthService,
          useClass: GoogleAuthServiceMock
        },
        {
          provide: 'BACKEND_API_URL',
          useValue: baseUrl
        },
        {
          provide: Router,
          useClass: RouterMock
        },
        {
          provide: NG_GAPI_CONFIG,
          useValue: config
        },
      ]
    });
    guard = TestBed.inject(RoleGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should call isUserAdmin() and return true if user is admin', inject([AuthService], (authService: AuthService) => {
    router = new MockRouter();
    roleGuard = new RoleGuard(router, authService);
    authService.isLoggedIn = true;

    spyOn(roleGuard, 'isUserAdmin').and.returnValue( of (true));
    roleGuard.canActivate(routeMock, routeStateMock).subscribe(
      response => {
        expect(roleGuard.isUserAdmin).toHaveBeenCalled();
        expect(response).toEqual(true);
      }
    );
  }));

  it('should call isUserAdmin() and return false if user is not admin', inject([AuthService], (authService: AuthService) => {
    router = new MockRouter();
    roleGuard = new RoleGuard(router, authService);
    authService.isLoggedIn = true;

    spyOn(roleGuard, 'isUserAdmin').and.returnValue( of (false));
    roleGuard.canActivate(routeMock, routeStateMock).subscribe(
      response => {
        expect(roleGuard.isUserAdmin).toHaveBeenCalled();
        expect(response).toEqual(false);
      }
    );
  }));

  it('should call userIsNotLoggedIn() and return false if user is not logged in', inject([AuthService], (authService: AuthService) => {
    router = new MockRouter();
    roleGuard = new RoleGuard(router, authService);
    authService.isLoggedIn = false;

    spyOn(roleGuard, 'userIsNotLoggedIn').and.returnValue( of (false));
    roleGuard.canActivate(routeMock, routeStateMock).subscribe(
      response => {
        expect(roleGuard.userIsNotLoggedIn).toHaveBeenCalled();
        expect(response).toEqual(false);
      }
    );
  }));

  it('should return true if user is admin', inject([AuthService], (authService: AuthService) => {
    spyOn(authService, 'getLoggedinUser').and.returnValue( of (getMockUsers()[0]));
    guard.isUserAdmin().subscribe(
      response => {
        expect(response).toEqual(true);
      }
    );
  }));

  it('should return false if user is not admin', inject([AuthService], (authService: AuthService) => {
    spyOn(authService, 'getLoggedinUser').and.returnValue( of (getMockUsers()[2]));
    guard.isUserAdmin().subscribe(
      response => {
        expect(response).toEqual(false);
      }
    );
  }));

  it('should return false and navigate to unauthorized route', inject([AuthService], (authService: AuthService) => {
    router = new MockRouter();
    roleGuard = new RoleGuard(router, authService);
    spyOn(router, 'navigate');
    roleGuard.userIsNotLoggedIn().subscribe(
      response => {
        expect(response).toEqual(false);
      }
    );
    expect(router.navigate).toHaveBeenCalledWith(['/unauthorized']);
  }));
});
