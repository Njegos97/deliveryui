import {
  HttpClientTestingModule
} from '@angular/common/http/testing';
import {
  TestBed
} from '@angular/core/testing';
import {
  Router
} from '@angular/router';
import {
  NgGapiClientConfig
} from 'ng-gapi/lib/src/config/GoogleApiConfig';
import {
  NG_GAPI_CONFIG
} from 'ng-gapi/lib/src/GoogleApiService';
import {
  GoogleAuthService
} from 'ng-gapi/lib/src/GoogleAuthService';
import {
  GoogleAuthServiceMock
} from '../test-helpers/google-auth-service-mock';
import {
  RouterMock
} from '../test-helpers/router-mock';

import {
  AuthGuardService
} from './auth-guard.service';

describe('AuthGuardService', () => {
  let service: AuthGuardService;
  const baseUrl = 'http://localhost:8080/api';

  const config: NgGapiClientConfig = {
    discoveryDocs: []
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
          provide: 'BACKEND_API_URL',
          useValue: baseUrl
        },
        {
          provide: GoogleAuthService,
          useClass: GoogleAuthServiceMock
        },
        {
          provide: NG_GAPI_CONFIG,
          useValue: config
        },
        {
          provide: Router,
          useClass: RouterMock
        }
      ]
    });
    service = TestBed.inject(AuthGuardService);
  });


});
