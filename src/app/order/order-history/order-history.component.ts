import {
  AuthService
} from './../../service/auth.service';
import {
  OrderService
} from './../../service/order.service';
import {
  Order
} from './../../model/order.model';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  Cart
} from 'src/app/model/cart.model';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  orders: Order[] = [];
  orderContent: Cart[] = [];
  displayedColumns: string[] = ['Food', 'Type', 'Price', 'Food Quantity'];

  constructor(private orderService: OrderService) {}

  ngOnInit(): void {
    this.getOrderHistory();
  }

  getOrder(id: number) {
    this.orderService.getOrderContent(id).subscribe((response: Cart[]) => {
      this.orderContent = response;
    });
  }

  getOrderHistory() {
    this.orderService.getOrderHistory().subscribe((orders: Order[]) => {
      this.orders = orders;
    });
  }



}
