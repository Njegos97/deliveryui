import { getMockOrders } from './../../mock/mock-order';
import { OrderService } from './../../service/order.service';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { OrderHistoryComponent } from './order-history.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable, of } from 'rxjs';
import { Order } from 'src/app/model/order.model';
import { getMockCart } from 'src/app/mock/mock-cart';
import { Cart } from 'src/app/model/cart.model';

describe('OrderHistoryComponent', () => {
  let component: OrderHistoryComponent;
  let fixture: ComponentFixture<OrderHistoryComponent>;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderHistoryComponent ],
      imports: [ HttpClientTestingModule],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl}]
    })
    .overrideTemplate(OrderHistoryComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should getAllOrders when getHistoryOfOrders is called', inject([OrderService], (orderService: OrderService) => {
    const orders: Observable<Order[]> = of(getMockOrders());
    spyOn(orderService, 'getOrderHistory').and.returnValue(orders);

    component.getOrderHistory();

    expect(orderService.getOrderHistory).toHaveBeenCalled();
  }));

  it('should getOrderContent when getOrder is called', inject([OrderService], (orderService: OrderService) => {
    const orderContent: Observable<Cart> = of(getMockCart()[0]);
    const idOfOrder = 1;
    spyOn(orderService, 'getOrderContent').and.returnValue(orderContent);

    component.getOrder(idOfOrder);

    expect(orderService.getOrderContent).toHaveBeenCalled();
  }));
});
