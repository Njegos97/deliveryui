import { MaterialModule } from './../material/material.module';
import { OrderRoutingModule } from './order-routing.module';
import {
  OrderComponent
} from './order/order.component';
import {
  NgModule
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { OrderHistoryComponent } from './order-history/order-history.component';
import {MatInputModule} from '@angular/material/input';
import { FormsModule} from '@angular/forms';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import * as cloudinary from 'cloudinary-core';
const customNotifierOptions: NotifierOptions = {
  behaviour: {
    autoHide: 1000,
  }
};

@NgModule({
  declarations: [OrderComponent, OrderHistoryComponent],
  imports: [
    CommonModule,
    OrderRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    NotifierModule,
    MatInputModule,
    FormsModule,
    CloudinaryModule.forRoot(cloudinary, {cloud_name: 'bgs-delivery'}),
  ]

})
export class OrderModule {}
