import { AuthGuardService } from './../guard/auth-guard.service';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { Routes, RouterModule } from '@angular/router';
import { OrderComponent } from './order/order.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
    { path: 'order', component: OrderComponent, },
    { path: 'order-history', component: OrderHistoryComponent, canActivate: [AuthGuardService]},
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class OrderRoutingModule { }
