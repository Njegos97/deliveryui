import {
  NotifierService
} from 'angular-notifier';
import {
  OrderService
} from './../../service/order.service';
import {
  SharedService
} from './../../service/shared.service';
import {
  Component,
  OnInit,
  Input,
  OnDestroy
} from '@angular/core';
import {
  Cart
} from 'src/app/model/cart.model';
import {
  Router
} from '@angular/router';
import {
  Subscription
} from 'rxjs';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit, OnDestroy {

  sentTo = '';
  orderPrice = 0;
  cart: Cart[] = [];
  subscription: Subscription;
  constructor(private sharedService: SharedService,
              private orderService: OrderService,
              private notifierService: NotifierService,
              private router: Router) {}

  ngOnInit(): void {
    this.subscription = this.sharedService.cartFood.subscribe(cart => this.cart = cart);
    this.calculateOrderPrice();
    this.convertDescription();

  }

  orderFood(sentToValue: string) {
    this.sentTo = sentToValue;
    this.orderService.addNewOrder(this.orderPrice, this.cart, this.sentTo).subscribe(() => {
      setTimeout(() => {
        this.router.navigate([`/food-list`]);
      }, 1000);
      this.emptyCartAfterOrder();
      this.sharedService.resetCartItemsAfterOrder();
      this.notifierService.notify('success', 'Order on the way!');
    });
  }

  private emptyCartAfterOrder(){
    this.subscription = this.sharedService.cartFood.subscribe(cart => cart.splice(0, cart.length));
  }

  calculateOrderPrice(): number {
    for (const cartContent of this.cart) {
      this.orderPrice += cartContent.food.price * cartContent.foodQuantity;
    }
    return parseFloat(this.orderPrice.toFixed(2));
  }

  reduceFoodQuantity(orderFood: Cart) {
    orderFood.foodQuantity--;
    this.orderPrice -= orderFood.food.price;
    this.sharedService.decrementCartItemsNumber();
  }

  increaseFoodQuantity(orderFood: Cart) {
    orderFood.foodQuantity++;
    this.orderPrice += orderFood.food.price;
    this.sharedService.incrementCartItemsNumber();
  }

  removeFromOrder(orderFood: Cart) {
    const index = this.cart.indexOf(orderFood);
    this.cart.splice(index, 1);
    this.orderPrice -= orderFood.food.price * orderFood.foodQuantity;
    this.sharedService.decrementCartItemsNumberBy(orderFood.foodQuantity);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  convertDescription(){
    for (const food of this.cart){
      const description = food.food.description.toString();
      food.food.descriptionArray =  description.split(',');
    }

  }


}
