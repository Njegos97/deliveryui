import { getMockFoodList } from './../../mock/mock-food-list';
import { getMockCart } from './../../mock/mock-cart';
import { OrderService } from './../../service/order.service';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { OrderComponent } from './order.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, Observable } from 'rxjs';
import { Cart } from 'src/app/model/cart.model';
import { NotifierService, NotifierModule } from 'angular-notifier';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterMock } from 'src/app/test-helpers/router-mock';
import { ActivatedRouteMock } from 'src/app/test-helpers/activated-route-mock';


describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;
  const baseUrl = 'http://localhost:8080/api';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderComponent ],
      imports: [ HttpClientTestingModule, RouterTestingModule, NotifierModule],
      providers: [{provide: 'BACKEND_API_URL', useValue: baseUrl},
      {provide: NotifierService, useClass: NotifierService},
      { provide: Router, useClass: RouterMock },
      { provide: ActivatedRoute, useClass: ActivatedRouteMock },
    ]
    })
    .overrideTemplate(OrderComponent, '').compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call orderService orderFood() when component.orderFood() is called', inject([OrderService], (orderService: OrderService) => {
    const cart: Observable<Cart[]> = of(getMockCart());
    const sentTo = 'Liberty street';
    spyOn(orderService, 'addNewOrder').and.returnValue(cart);

    component.orderFood(sentTo);

    expect(orderService.addNewOrder).toHaveBeenCalledWith(component.orderPrice, component.cart, component.sentTo);
  }));

  it('should reduce foodQuantity ', () => {
    const cart: Cart = getMockCart()[0];
    cart.foodQuantity = 2;

    component.reduceFoodQuantity(cart);

    expect(cart.foodQuantity).toEqual(1);
  });

  it('should increase foodQuantity ', () => {
    const cart: Cart = getMockCart()[0];
    cart.foodQuantity = 2;

    component.increaseFoodQuantity(cart);

    expect(cart.foodQuantity).toEqual(3);
  });

  it('should remove from cart ', () => {
    const cart: Cart[] = getMockCart();
    const cartContent: Cart = getMockCart()[0];
    const index = cart.indexOf(cartContent);

    cart.splice(index, 1);
    component.removeFromOrder(cart[0]);

    expect(cart.length).toEqual(1);
  });

});
