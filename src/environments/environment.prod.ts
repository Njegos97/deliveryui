export const environment = {
  production: true,
  backendApiUrl: '${BACKEND_API_URL}',
  googleClientId: '41740491435-iq0bba70nmtjcsef0ru1l1l3jqk7n1g4.apps.googleusercontent.com'
};
